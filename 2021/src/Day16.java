import com.google.common.base.Preconditions;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Iterables.getOnlyElement;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day16 {
    public static long part1(URL url) throws IOException {
        return part1(Resources.readLines(url, StandardCharsets.UTF_8).get(0));
    }

    static long part1(String hex) {
        return sum(parse(parseHex(hex)));
    }

    sealed interface Packet {
        int version();
        int typeId();
        record Literal(int version, int typeId, long value) implements Packet {}
        record Operator(int version, int typeId, int lengthTypeId, List<Packet> subpackets) implements Packet {}
    }

    static long sum(Packet packet) {
        return packet.version() + switch (packet) {
            case Packet.Literal l -> 0L;
            case Packet.Operator o -> o.subpackets.stream().mapToLong(Day16::sum).sum();
        };
    }

    record ParseResult(List<Packet> packet, int nextPosition) {}
    static Packet parse(List<Short> data) {
        return getOnlyElement(parse1(data, 0).packet);
    }

    static ParseResult parse1(List<Short> data, int pos) {
        int version = bits(data, pos, 3);
        int typeId = bits(data, pos+3, 3);
        if (typeId == 4) {
            long result = 0L;
            short value;
            int nextPos = pos+6;
            do {
                value = bits(data, nextPos, 5);
                result <<= 4;
                result |= value & 0b1111;
                nextPos += 5;
            } while ((value & 0b10000) != 0);
            Preconditions.checkState(nextPos - pos <= 86, "overflowed long");
            return new ParseResult(List.of(new Packet.Literal(version, typeId, result)), nextPos);
        } else {
            int lengthTypeId = bits(data, pos+6, 1);
            if (lengthTypeId == 0) {
                int length = bits(data, pos+7, 15);
                ParseResult subpacketResult = parseByLength(data, pos+22, length);
                return new ParseResult(List.of(new Packet.Operator(version, typeId, lengthTypeId, subpacketResult.packet)), subpacketResult.nextPosition);
            } else {
                int count = bits(data, pos+7, 11);
                ParseResult subpacketResult = parseByNumber(data, pos+18, count);
                return new ParseResult(List.of(new Packet.Operator(version, typeId, lengthTypeId, subpacketResult.packet)), subpacketResult.nextPosition);
            }
        }
    }

    static ParseResult parseByNumber(List<Short> data, int pos, int count) {
        List<Packet> results = new ArrayList<>();
        int nextPos = pos;
        while (count > 0) {
            ParseResult next = parse1(data, nextPos);
            nextPos = next.nextPosition;
            results.add(getOnlyElement(next.packet));
            count -= 1;
        }
        return new ParseResult(results, nextPos);
    }

    static ParseResult parseByLength(List<Short> data, int pos, int length) {
        List<Packet> results = new ArrayList<>();
        int nextPos = pos;
        while (nextPos < pos + length) {
            ParseResult next = parse1(data, nextPos);
            nextPos = next.nextPosition;
            results.add(getOnlyElement(next.packet));
        }
        Preconditions.checkState(nextPos == pos + length);
        return new ParseResult(results, nextPos);
    }

    static List<Short> parseHex(String s) {
            if (s.length() % 2 != 0) {
                throw new RuntimeException("unimplemented");
            }
            List<Short> result = new ArrayList<>();
            for (int i = 0; i < s.length(); i += 2) {
                result.add(Short.parseShort(s.substring(i, i+2), 16));
            }
            return result;
    }

    static short bits(List<Short> data, int offset, int count) {
        Preconditions.checkArgument(offset + count <= 8 * data.size());
        int index = offset / 8;
        short result = 0;
        int bitOffset = 8 - offset % 8;
        while (count > 0) {
            int bitCount = Math.min(count, bitOffset);
            result <<= bitCount;
            int mask = (1 << bitCount) - 1;
            short value = data.get(index);
            result |= (short) ((value >> (bitOffset - bitCount)) & mask);
            index += 1;
            bitOffset = 8;
            count -= bitCount;
        }
        return result;
    }

    @Test
    public void testBits() throws Exception {
        assertEquals(0b111, bits(List.of((short) 0b11111111), 0, 3));
        assertEquals(0b010101010101, bits(List.of((short) 0b10101010, (short) 0b10101010, (short) 0b10101010), 5, 12));
        assertEquals(0b11111111, bits(List.of((short) 0b101010101, (short) 0b11111111, (short) 0b10101010), 8, 8));
        assertEquals(0, bits(List.of((short) 0b101010101), 0, 0));
    }

    public static long part2(URL url) throws IOException {
        return part2(Resources.readLines(url, StandardCharsets.UTF_8).get(0));
    }

    static long part2(String hex) {
        return value(parse(parseHex(hex)));
    }

    static long value(Packet packet) {
        return switch (packet) {
            case Packet.Literal l -> l.value();
            case Packet.Operator o -> switch (o.typeId) {
                case 0 -> o.subpackets.stream().mapToLong(Day16::value).sum();
                case 1 -> o.subpackets.stream().mapToLong(Day16::value).reduce((x, y) -> x*y).getAsLong();
                case 2 -> o.subpackets.stream().mapToLong(Day16::value).min().getAsLong();
                case 3 -> o.subpackets.stream().mapToLong(Day16::value).max().getAsLong();
                case 5 -> {
                    Preconditions.checkState(o.subpackets.size() == 2);
                    yield value(o.subpackets.get(0)) > value(o.subpackets.get(1)) ? 1 : 0;
                }
                case 6 -> {
                    Preconditions.checkState(o.subpackets.size() == 2);
                    yield value(o.subpackets.get(0)) < value(o.subpackets.get(1)) ? 1 : 0;
                }
                case 7 -> {
                    Preconditions.checkState(o.subpackets.size() == 2);
                    yield value(o.subpackets.get(0)) == value(o.subpackets.get(1)) ? 1 : 0;
                }
                default -> throw new IllegalStateException("unimplemented");
            };
        };
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(16L, part1("8A004A801A8002F478"));
        assertEquals(12L, part1("620080001611562C8802118E34"));
        assertEquals(23L, part1("C0015000016115A2E0802F182340"));
        assertEquals(31L, part1("A0016C880162017C3686B18A3D4780"));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(1014L, part1(Resources.getResource(getClass(), "16.txt")));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(3L, part2("C200B40A82"));
        assertEquals(54L, part2("04005AC33890"));
        assertEquals(7L, part2("880086C3E88112"));
        assertEquals(9L, part2("CE00C43D881120"));
        assertEquals(1L, part2("D8005AC2A8F0"));
        assertEquals(0L, part2("F600BC2D8F"));
        assertEquals(0L, part2("9C005AC2F8F0"));
        assertEquals(1L, part2("9C0141080250320F1802104A08"));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(1922490999789L, part2(Resources.getResource(getClass(), "16.txt")));
    }
}
