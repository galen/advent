import com.google.common.base.Preconditions;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day14 {
    public static long part1(URL url) throws IOException {
        Input input = parse(url);
        String pattern = input.template;
        for (int i = 0; i < 10; i++) {
            pattern = step(pattern, input.rules);
        }
        LongSummaryStatistics statistics = pattern.codePoints()
                .boxed()
                .collect(groupingBy(Function.identity(), counting()))
                .values()
                .stream()
                .mapToLong(Long::longValue)
                .summaryStatistics();
        return statistics.getMax() - statistics.getMin();
    }

    record Input(String template, List<Rule> rules) {}
    record Rule(String pattern, String insertion) {}
    static Input parse(URL url) throws IOException {
        List<String> lines = Resources.readLines(url, StandardCharsets.UTF_8);
        Preconditions.checkState(lines.get(1).equals(""));
        return new Input(lines.get(0), lines.subList(2, lines.size()).stream()
                .map(line -> {
                    String[] parts = line.split(" -> ");
                    return new Rule(parts[0], parts[1]);
                })
                .collect(Collectors.toList()));
    }

    static String step(String current, List<Rule> rules) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < current.length(); i++) {
            sb.appendCodePoint(current.codePointAt(i));
            if (i < current.length() - 1) {
                int j = i;
                Optional<Rule> maybeRule = rules.stream()
                        .filter(r -> r.pattern.equals(current.substring(j, j+2)))
                        .findFirst();
                if (maybeRule.isPresent()) {
                    sb.append(maybeRule.get().insertion);
                }
            }
        }
        return sb.toString();
    }

    public static long part2(URL url) throws IOException {
        Input input = parse(url);
        Map<String, Long> counts = new HashMap<>();
        for (int i = 0; i < input.template.length() - 1; i++) {
            counts.merge(input.template.substring(i, i+2), 1L, Math::addExact);
        }
        String tail = input.template.substring(input.template.length() - 2);
        for (int i = 0; i < 40; i++) {
            Map<String, Long> nextCounts = new HashMap<>();
            for (Rule rule : input.rules) {
                long amountNow = counts.getOrDefault(rule.pattern, 0L);
                nextCounts.merge(rule.pattern.charAt(0) + rule.insertion, amountNow, Math::addExact);
                nextCounts.merge(rule.insertion + rule.pattern.charAt(1), amountNow, Math::addExact);
                if (tail.equals(rule.pattern)) {
                    tail = rule.insertion + rule.pattern.charAt(1);
                }
            }
            counts = nextCounts;
        }
        Map<Character, Long> collect = Stream.concat(Stream.of(Map.entry(tail.charAt(1), 1L)),
                counts.entrySet().stream().map(entry -> Map.entry(entry.getKey().charAt(0), entry.getValue())))
                .collect(groupingBy(Map.Entry::getKey, Collectors.reducing(0L, Map.Entry::getValue, Long::sum)));
        LongSummaryStatistics statistics = collect.values().stream()
                .mapToLong(Long::longValue)
                .summaryStatistics();
        return statistics.getMax() - statistics.getMin();
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(1588L, part1(Resources.getResource(getClass(), "14sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(3247L, part1(Resources.getResource(getClass(), "14.txt")));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(2188189693529L, part2(Resources.getResource(getClass(), "14sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(4110568157153L, part2(Resources.getResource(getClass(), "14.txt")));
    }
}
