import com.google.common.base.Preconditions;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day18 {
    public static long part1(URL url) throws IOException {
        return parse(url).stream()
                .reduce(Element::add)
                .map(Element::magnitude)
                .get();
    }

    sealed interface Element {
        default Element add(Element that) {
            return new Pair(this, that).reduce();
        }
        default Element reduce() {
            Element result = this;
            while (true) {
                Optional<Element> next = result.tryExplode().or(result::trySplit);
                if (next.isPresent()) {
                    result = next.get();
                } else {
                    return result;
                }
            }
        }
        private Optional<Element> tryExplode() {
            return Optional.of(this.explodeSearch(0)).filter(Explosion::didExplode).map(Explosion::rebuiltTree);
        }

        record Explosion(Optional<Regular> leftExploder, Optional<Regular> rightExploder, Element rebuiltTree, boolean didExplode) {}
        private Explosion explodeSearch(int level) {
            if (level == 4 && this instanceof Pair p) {
                if (!(p.left instanceof Regular l) || !(p.right instanceof Regular r)) {
                    throw new IllegalStateException("should be regular");
                }
                return new Explosion(Optional.of(l), Optional.of(r), new Regular(0), true);
            } else if (this instanceof Pair p) {
                Explosion leftExplosion = p.left.explodeSearch(level+1);
                Element right = p.right;
                if (leftExplosion.didExplode && leftExplosion.rightExploder.isPresent()) {
                    right = p.right.performRightAdd(leftExplosion.rightExploder.get());
                }
                Element left = leftExplosion.rebuiltTree;
                Explosion rightExplosion = null;
                if (!leftExplosion.didExplode) {
                    rightExplosion = p.right.explodeSearch(level + 1);
                    right = rightExplosion.rebuiltTree;
                    if (rightExplosion.didExplode && rightExplosion.leftExploder.isPresent()) {
                        left = left.performLeftAdd(rightExplosion.leftExploder.get());
                    }
                }
                return new Explosion(leftExplosion.leftExploder, Optional.ofNullable(rightExplosion).flatMap(Explosion::rightExploder), new Pair(left, right), leftExplosion.didExplode || rightExplosion.didExplode);
            } else {
                return new Explosion(Optional.empty(), Optional.empty(), this, false);
            }
        }
        private Element performLeftAdd(Regular n) {
            return switch(this) {
                case Pair p -> new Pair(p.left, p.right.performLeftAdd(n));
                case Regular r -> new Regular(r.number + n.number);
            };
        }
        private Element performRightAdd(Regular n) {
            return switch(this) {
                case Pair p -> new Pair(p.left.performRightAdd(n), p.right);
                case Regular r -> new Regular(r.number + n.number);
            };
        }
        private Optional<Element> trySplit() {
            return switch(this) {
                case Pair p -> {
                    Optional<Element> left = p.left.trySplit();
                    if (left.isPresent()) {
                        yield Optional.of(new Pair(left.get(), p.right));
                    } else {
                        yield p.right.trySplit().map(r -> new Pair(p.left, r));
                    }
                }
                case Regular r -> {
                    if (r.number < 10) {
                        yield Optional.empty();
                    } else {
                        yield Optional.of(new Pair(new Regular(r.number / 2), new Regular(r.number / 2 + r.number % 2)));
                    }
                }
            };
        }

        default int magnitude() {
            return switch(this) {
                case Pair p -> 3*p.left.magnitude() + 2*p.right.magnitude();
                case Regular r -> r.number;
            };
        }
    }
    record Pair(Element left, Element right) implements Element {}
    record Regular(int number) implements Element {}

    static List<Element> parse(URL url) throws IOException {
        return Resources.readLines(url, StandardCharsets.UTF_8)
                .stream()
                .map(Day18::parse)
                .collect(Collectors.toList());
    }

    static Element parse(String s) {
        return parse(s, 0).value;
    }

    record Result(Element value, int pos) {}
    static Pattern PATTERN = Pattern.compile("[0-9]+");
    static Result parse(String s, int pos) {
        if (s.charAt(pos) == '[') {
            Result r1 = parse(s, pos+1);
            Preconditions.checkState(s.charAt(r1.pos) == ',');
            Result r2 = parse(s, r1.pos + 1);
            Preconditions.checkState(s.charAt(r2.pos) == ']');
            return new Result(new Pair(r1.value, r2.value), r2.pos + 1);
        } else {
            Matcher matcher = PATTERN.matcher(s.substring(pos));
            if (!matcher.lookingAt()) {
                throw new IllegalStateException("no match");
            }
            return new Result(new Regular(Integer.parseInt(s.substring(pos, pos + matcher.end()))), pos + matcher.end());
        }
    }

    public static long part2(URL url) throws IOException {
        List<Element> elements = parse(url);

        return elements.stream()
                .flatMap(e1 -> elements.stream()
                        .flatMap(e2 -> e1 == e2 ? Stream.of() : Stream.of(e1.add(e2).magnitude())))
                .max(Comparator.naturalOrder())
                .get();
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(4140L, part1(Resources.getResource(getClass(), "18sample.txt")));
    }

    @Test
    public void test1Magnitude() throws Exception {
        assertEquals(143, parse("[[1,2],[[3,4],5]]").magnitude());
        assertEquals(1384, parse("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]").magnitude());
        assertEquals(445, parse("[[[[1,1],[2,2]],[3,3]],[4,4]]").magnitude());
        assertEquals(791, parse("[[[[3,0],[5,3]],[4,4]],[5,5]]").magnitude());
        assertEquals(1137, parse("[[[[5,0],[7,4]],[5,5]],[6,6]]").magnitude());
        assertEquals(3488, parse("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]").magnitude());
    }

    @Test
    public void test1Explode() throws Exception {
        assertEquals(parse("[[[[0,9],2],3],4]"), parse("[[[[[9,8],1],2],3],4]").tryExplode().get());
        assertEquals(parse("[7,[6,[5,[7,0]]]]"), parse("[7,[6,[5,[4,[3,2]]]]]").tryExplode().get());
        assertEquals(parse("[[6,[5,[7,0]]],3]"), parse("[[6,[5,[4,[3,2]]]],1]").tryExplode().get());
        assertEquals(parse("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"), parse("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]").tryExplode().get());
        assertEquals(parse("[[3,[2,[8,0]]],[9,[5,[7,0]]]]"), parse("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]").tryExplode().get());
    }

    @Test
    public void test1() throws Exception {
        assertEquals(3763L, part1(Resources.getResource(getClass(), "18.txt")));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(3993L, part2(Resources.getResource(getClass(), "18sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(4664L, part2(Resources.getResource(getClass(), "18.txt")));
    }
}
