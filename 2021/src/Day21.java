import com.google.common.base.Splitter;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day21 {
    public static long part1(URL url) throws IOException {
        State state = initial(parse(url));
        int rolls = 0;
        while (true) {
            Optional<Integer> winner = getWinner(state, 1000);
            if (winner.isPresent()) {
                return state.scores.get(1 - winner.get()) * rolls;
            }
            state = stepDeterministic(state, rolls, rolls % 2);
            rolls += 3;
        }
    }

    static Optional<Integer> getWinner(State state, int target) {
        return state.scores.entrySet().stream().filter(e -> e.getValue() >= target).findFirst().map(Map.Entry::getKey);
    }

    static List<Integer> parse(URL url) throws IOException {
        return Resources.readLines(url, StandardCharsets.UTF_8).stream()
                .map(s -> Integer.parseInt(Splitter.on(" ").splitToList(s).get(4)) - 1)
                .collect(Collectors.toList());
    }

    record State(Map<Integer, Integer> spaces, Map<Integer, Long> scores) {}

    static State initial(List<Integer> starting) {
        return new State(
                Map.of(0, starting.get(0), 1, starting.get(1)),
                Map.of(0, 0L, 1, 0L)
        );
    }

    static State stepDeterministic(State state, int rolls, int player) {
        int toMove = 3 + (rolls % 100) + (rolls + 1) % 100 + (rolls + 2) % 100;
        return step(state, player, toMove);
    }

    static State step(State state, int player, int toMove) {
        int nextSquare = (state.spaces.get(player) + toMove) % 10;
        Map<Integer, Integer> nextSpaces = update(state.spaces, player, nextSquare);
        Map<Integer, Long> nextScores = update(state.scores, player, state.scores.get(player) + nextSquare + 1);
        return new State(nextSpaces, nextScores);
    }

    static <K, V> Map<K,V> update(Map<K, V> map, K k, V v) {
        HashMap<K, V> temp = new HashMap<>(map);
        temp.put(k, v);
        return Map.copyOf(temp);
    }

    static final List<Integer> COEFFICIENTS = List.of(1, 3, 6, 7, 6, 3, 1);

    public static long part2(URL url) throws IOException {
        Map<State, Long> stateCounts = Map.of(initial(parse(url)), 1L);
        Map<Integer, Long> winners = new HashMap<>();
        int player = 0;
        while (!stateCounts.isEmpty()) {
            Map<State, Long> nextStateCounts = new HashMap<>();
            for (Map.Entry<State, Long> entry : stateCounts.entrySet()) {
                Optional<Integer> winner = getWinner(entry.getKey(), 21);
                if (winner.isEmpty()) {
                    for (int x = 0; x < COEFFICIENTS.size(); x++) {
                        nextStateCounts.merge(step(entry.getKey(), player, 3 + x), COEFFICIENTS.get(x) * entry.getValue(), Math::addExact);
                    }
                } else {
                    winners.merge(winner.get(), entry.getValue(), Math::addExact);
                }
            }
            stateCounts = nextStateCounts;
            player = 1 - player;
        }
        return winners.entrySet().stream()
                .max(Map.Entry.comparingByValue())
                .get()
                .getValue();
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(739785L, part1(Resources.getResource(getClass(), "21sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(506466L, part1(Resources.getResource(getClass(), "21.txt")));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(444356092776315L, part2(Resources.getResource(getClass(), "21sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(632979211251440L, part2(Resources.getResource(getClass(), "21.txt")));
    }
}
