import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day17 {
    record Point(int x, int y) {}
    record State(Point location, Point velocity) {
        State step() {
            return new State(new Point(location.x + velocity.x, location.y + velocity.y),
                    new Point(velocity.x + (velocity.x < 0 ? 1 : velocity.x > 0 ? -1 : 0),
                            velocity.y - 1));
        }
    }

    record StateHistory(State state, int height) {
        static Optional<Integer> run(Point initialVelocity, Target target) {
            StateHistory state = new StateHistory(new State(new Point(0, 0), initialVelocity), 0);
            while (state.state.location.y >= target.y.min) {
                if (target.within(state.state.location)) {
                    return Optional.of(state.height);
                }
                State next = state.state.step();
                state = new StateHistory(next, Math.max(next.location.y, state.height));
            }
            return Optional.empty();
        }
    }

    record Range(int min, int max) {}

    record Target(Range x, Range y) {
        boolean within(Point location) {
            return x.min <= location.x && location.x <= x.max && y.min <= location.y && location.y <= y.max;
        }
    }

    public static int part1(Target target) {
        return IntStream.rangeClosed(0, target.x.max)
                .mapToObj(x -> IntStream.rangeClosed(target.y.min, target.x.max)
                        .mapToObj(y -> StateHistory.run(new Point(x, y), target))
                        .flatMap(Optional::stream))
                .flatMap(Function.identity())
                .max(Comparator.naturalOrder())
                .get();
    }

    public static long part2(Target target) {
        return IntStream.rangeClosed(0, target.x.max)
                .mapToObj(x -> IntStream.rangeClosed(target.y.min, target.x.max)
                        .mapToObj(y -> StateHistory.run(new Point(x, y), target))
                        .flatMap(Optional::stream))
                .flatMap(Function.identity())
                .count();
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(45, part1(new Target(new Range(20, 30), new Range(-10, -5))));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(5995, part1(new Target(new Range(156, 202), new Range(-110, -69))));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(112L, part2(new Target(new Range(20, 30), new Range(-10, -5))));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(3202L, part2(new Target(new Range(156, 202), new Range(-110, -69))));
    }
}
