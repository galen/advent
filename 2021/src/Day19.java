import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day19 {
    public static long part1(URL url) throws IOException {
        SetMultimap<Integer, Triple> scannerData = parse(url);
        Map<Integer, Configuration> scannerConfigurations = computeScannerConfigurations(scannerData);

        return scannerData.entries().stream().map(e -> scannerConfigurations.get(e.getKey()).apply(e.getValue())).collect(Collectors.toSet()).size();
    }

    private static Map<Integer, Configuration> computeScannerConfigurations(SetMultimap<Integer, Triple> scannerData) {
        SetMultimap<Integer, RelativeDistance> relativeDistances = relativeDistances(scannerData);
        Map<Integer, Configuration> scannerConfigurations = new HashMap<>();
        Deque<Integer> remainingUnknowns = new ArrayDeque<>(scannerData.keySet());

        remainingUnknowns.remove(0);
        scannerConfigurations.put(0, new Configuration(Orientation.trivial(), new Triple(0, 0, 0)));

        int watch = 0;
        SEARCH: while (!remainingUnknowns.isEmpty()) {
            if (watch++ >= remainingUnknowns.size()) {
                throw new RuntimeException("failed to progress");
            }
            Integer unknown = remainingUnknowns.removeFirst();
            for (Map.Entry<Integer, Configuration> knownEntry : scannerConfigurations.entrySet()) {
                Integer known = knownEntry.getKey();
                Configuration knownConfiguration = knownEntry.getValue();
                for (Orientation orientation : Orientation.orientations()) {
                    Map<Triple, List<RelativeDistance>> unknownRelativeDistances = relativeDistances.get(unknown).stream().map(orientation::apply).collect(Collectors.groupingBy(RelativeDistance::distance, Collectors.mapping(Function.identity(), Collectors.toList())));
                    Map<Triple, List<RelativeDistance>> knownRelativeDistances = relativeDistances.get(known).stream().map(knownConfiguration.orientation::apply).collect(Collectors.groupingBy(RelativeDistance::distance, Collectors.mapping(Function.identity(), Collectors.toList())));
                    Set<Triple> intersection = Sets.intersection(unknownRelativeDistances.keySet(), knownRelativeDistances.keySet());
                    if (intersection.size() >= 132) {
                        for (Triple r : intersection) {
                            for (RelativeDistance rt : unknownRelativeDistances.get(r)) {
                                for (RelativeDistance rs : knownRelativeDistances.get(r)) {
                                    Triple delta = knownConfiguration.orientation.apply(rs.t).difference(orientation.apply(rt.t));
                                    Triple distance = knownConfiguration.location.add(delta);
                                    Configuration hypothesizedConfiguration = new Configuration(orientation, distance);
                                    Set<Triple> knownAbsoluteLocations = scannerData.get(known).stream().map(knownConfiguration).collect(Collectors.toSet());
                                    Set<Triple> unknownAbsoluteLocations = scannerData.get(unknown).stream().map(hypothesizedConfiguration).collect(Collectors.toSet());
                                    if (Sets.intersection(knownAbsoluteLocations, unknownAbsoluteLocations).size() >= 12) {
                                        scannerConfigurations.put(unknown, hypothesizedConfiguration);
                                        watch = 0;
                                        continue SEARCH;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            remainingUnknowns.addLast(unknown);
        }
        return scannerConfigurations;
    }

    record RelativeDistance(Triple t, Triple s, Triple distance) {
        static RelativeDistance from(Triple t, Triple s) {
            return new RelativeDistance(t, s, t.difference(s));
        }
    }

    static SetMultimap<Integer, RelativeDistance> relativeDistances(SetMultimap<Integer, Triple> scannerData) {
        SetMultimap<Integer, RelativeDistance> relativeDistances = HashMultimap.create();
        for (int i : scannerData.keys()) {
            for (Triple t : scannerData.get(i)) {
                for (Triple s : scannerData.get(i)) {
                    if (!t.equals(s)) {
                        relativeDistances.put(i, RelativeDistance.from(t, s));
                    }
                }
            }
        }
        return ImmutableSetMultimap.copyOf(relativeDistances);
    }

    record Orientation(List<List<Long>> elements) {
        Triple apply(Triple coordinates) {
            return new Triple(
                    get(0, 0) * coordinates.x + get(0, 1) * coordinates.y + get(0, 2) * coordinates.z,
                    get(1, 0) * coordinates.x + get(1, 1) * coordinates.y + get(1, 2) * coordinates.z,
                    get(2, 0) * coordinates.x + get(2, 1) * coordinates.y + get(2, 2) * coordinates.z
            );
        }

        // units are quarter-turns
        // https://en.wikipedia.org/wiki/Rotation_matrix
        static Orientation of(int yaw, int pitch, int roll) {
            return new Orientation(List.of(
                    List.of(cos(yaw)*cos(pitch), cos(yaw)*sin(pitch)*sin(roll) - sin(yaw)*cos(roll), cos(yaw)*sin(pitch)*cos(roll) + sin(yaw)*sin(roll)),
                    List.of(sin(yaw)*cos(pitch), sin(yaw)*sin(pitch)*sin(roll) + cos(yaw)*cos(roll), sin(yaw)*sin(pitch)*cos(roll) - cos(yaw)*sin(roll)),
                    List.of(-sin(pitch), cos(pitch)*sin(roll), cos(pitch)*cos(roll))
            ));
        }

        // units are quarter-turns
        private static long sin(int theta) {
            return switch (theta) {
                case 0 -> 0L;
                case 1 -> 1L;
                case 2 -> 0L;
                case 3 -> -1L;
                default -> throw new IllegalArgumentException();
            };
        }

        private static long cos(int theta) {
            return sin((1+theta) % 4);
        }

        long get(int row, int col) {
            return elements.get(row).get(col);
        }

        public RelativeDistance apply(RelativeDistance relativeDistance) {
            return new RelativeDistance(relativeDistance.t, relativeDistance.s, apply(relativeDistance.distance));
        }

        static Set<Orientation> orientations() {
            List<Orientation> result = new ArrayList<>();
            for (int yaw = 0; yaw < 4; yaw++) {
                for (int pitch = 0; pitch < 4; pitch++) {
                    for (int roll = 0; roll < 4; roll++) {
                        result.add(of(yaw, pitch, roll));
                    }
                }
            }
            return Set.copyOf(result);
        }

        static Orientation trivial() {
            return new Orientation(List.of(List.of(1L, 0L, 0L), List.of(0L, 1L, 0L), List.of(0L, 0L, 1L)));
        }
    }

    record Configuration(Orientation orientation, Triple location) implements Function<Triple, Triple> {
        @Override
        public Triple apply(Triple beacon) {
            return location.add(orientation.apply(beacon));
        }
    }

    record Triple(long x, long y, long z) {
        static Triple parse(String line) {
            String[] fields = line.split(",");
            return new Triple(Long.parseLong(fields[0]), Long.parseLong(fields[1]), Long.parseLong(fields[2]));
        }

        Triple difference(Triple that) {
            return new Triple(this.x - that.x, this.y - that.y, this.z - that.z);
        }

        Triple add(Triple that) {
            return new Triple(this.x + that.x, this.y + that.y, this.z + that.z);
        }

        long manhattan() {
            return Math.abs(x) + Math.abs(y) + Math.abs(z);
        }
    }

    static SetMultimap<Integer, Triple> parse(URL url) throws IOException {
        SetMultimap<Integer, Triple> scannerData = HashMultimap.create();
        List<String> lines = Resources.readLines(url, StandardCharsets.UTF_8);
        OptionalInt scanner = OptionalInt.empty();
        for (String line : lines) {
            if (line.isEmpty()) {
                continue;
            }
            if (line.startsWith("---")) {
                String[] fields = line.split(" ");
                scanner = OptionalInt.of(Integer.parseInt(fields[2]));
                continue;
            }
            scannerData.put(scanner.getAsInt(), Triple.parse(line));
        }
        return ImmutableSetMultimap.copyOf(scannerData);
    }

    public static long part2(URL url) throws IOException {
        SetMultimap<Integer, Triple> scannerData = parse(url);
        Map<Integer, Configuration> scannerConfigurations = computeScannerConfigurations(scannerData);
        List<Triple> scannerLocations = scannerConfigurations.values().stream().map(Configuration::location).toList();

        return scannerLocations.stream()
                .flatMap(t -> scannerLocations.stream()
                        .map(t::difference)
                        .map(Triple::manhattan))
                .max(Comparator.naturalOrder())
                .get();
    }

    @Test
    public void testOrientation() {
        assertEquals(Orientation.of(0, 0, 0), Orientation.trivial());
        assertEquals(24, Orientation.orientations().size());
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(79L, part1(Resources.getResource(getClass(), "19sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(451L, part1(Resources.getResource(getClass(), "19.txt")));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(3621L, part2(Resources.getResource(getClass(), "19sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(13184L, part2(Resources.getResource(getClass(), "19.txt")));
    }
}
