import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day15 {
    public static long part1(URL url) throws IOException {
        return calculate(parse(url)).get(0).get(0);
    }

    static List<List<Integer>> parse(URL url) throws IOException {
        return Resources.readLines(url, StandardCharsets.UTF_8).stream()
                .map(s -> Arrays.stream(s.split("")).map(Integer::parseInt).collect(Collectors.toList()))
                .collect(Collectors.toList());
    }

    static List<List<Integer>> calculate(List<List<Integer>> map) {
        record Coordinates(int row, int col) {}
        record Step(int risk, Coordinates coordinates) {}
        List<List<Integer>> path = IntStream.range(0, map.size())
                .mapToObj(row -> IntStream.range(0, map.get(row).size())
                        .mapToObj(col -> (Integer) null)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
        PriorityQueue<Step> queue = new PriorityQueue<>(Comparator.comparing(Step::risk));
        queue.add(new Step(0, new Coordinates(map.size() - 1, map.get(map.size() - 1).size() - 1)));
        while (!queue.isEmpty()) {
            Step step = queue.poll();
            if (step.coordinates.row < 0 || step.coordinates.col < 0 || step.coordinates.row >= map.size() || step.coordinates.col >= map.get(step.coordinates.row).size()) {
                continue;
            }
            if (path.get(step.coordinates.row).get(step.coordinates.col) != null) {
                continue;
            }
            path.get(step.coordinates.row).set(step.coordinates.col, step.risk);
            int myRisk = map.get(step.coordinates.row).get(step.coordinates.col) + step.risk;
            for (Coordinates delta : List.of(new Coordinates(-1, 0), new Coordinates(0, -1), new Coordinates(1, 0), new Coordinates(0, 1))) {
                queue.add(new Step(myRisk, new Coordinates(step.coordinates.row + delta.row, step.coordinates.col + delta.col)));
            }
        }
        return path;
    }

    public static long part2(URL url) throws IOException {
        return calculate(expand(parse(url))).get(0).get(0);
    }

    private static List<List<Integer>> expand(List<List<Integer>> map) {
        return IntStream.range(0, 5)
                .mapToObj(bigRow -> map.stream()
                        .map(row -> IntStream.range(0, 5)
                                .mapToObj(bigCol -> row.stream()
                                        .map(item -> inc(item, bigRow + bigCol)))
                                .flatMap(Function.identity())
                                .collect(Collectors.toList())))
                .flatMap(Function.identity())
                .collect(Collectors.toList());
    }

    private static int inc(int x, int iterations) {
        while (iterations-- > 0) {
            x = x >= 9 ? 1 : x+1;
        }
        return x;
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(40L, part1(Resources.getResource(getClass(), "15sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(745L, part1(Resources.getResource(getClass(), "15.txt")));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(315L, part2(Resources.getResource(getClass(), "15sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(3002L, part2(Resources.getResource(getClass(), "15.txt")));
    }
}
