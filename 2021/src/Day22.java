import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.SetMultimap;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day22 {
    public static long part1(URL url) throws IOException {
        Map<Location, State> grid = new HashMap<>();
        for (Input input : Input.parse(url)) {
            applyCommand(grid, input);
        }
        return grid.values().stream()
                .filter(s -> s.equals(State.on))
                .count();
    }

    record Location(Map<Axis, Integer> coordinates) {}

    static void applyCommand(Map<Location, State> grid, Input input) {
        input.bounds.get(Axis.x).stream()
                .forEach(x -> input.bounds.get(Axis.y).stream()
                        .forEach(y -> input.bounds.get(Axis.z).stream()
                                .forEach(z -> grid.put(new Location(Map.of(Axis.x, x, Axis.y, y, Axis.z, z)), input.state))));
    }

    static boolean[] createBoolean(ListMultimap<Axis, Integer> gridlines) {
        return new boolean[gridlines.get(Axis.x).size() * gridlines.get(Axis.y).size() * gridlines.get(Axis.z).size()];
    }

    static int index(ListMultimap<Axis, Integer> gridlines, Location location) {
        return gridlines.get(Axis.y).size() * gridlines.get(Axis.z).size() * gridlines.get(Axis.x).indexOf(location.coordinates.get(Axis.x))
        + gridlines.get(Axis.z).size() * gridlines.get(Axis.y).indexOf(location.coordinates.get(Axis.y))
        + gridlines.get(Axis.z).indexOf(location.coordinates.get(Axis.z));
    }

    static boolean getBoolean(boolean[] grid, ListMultimap<Axis, Integer> gridlines, Location location) {
        return grid[index(gridlines, location)];
    }

    static void setBoolean(boolean[] grid, ListMultimap<Axis, Integer> gridlines, Location location, State state) {
        grid[index(gridlines, location)] = state == State.on;
    }

    static void applyCommand(boolean[] grid, ListMultimap<Axis, Integer> gridlines, Input input) {
        input.bounds.get(Axis.x).matchingGridlines(gridlines.get(Axis.x))
                .forEach(x -> input.bounds.get(Axis.y).matchingGridlines(gridlines.get(Axis.y))
                        .forEach(y -> input.bounds.get(Axis.z).matchingGridlines(gridlines.get(Axis.z))
                                .forEach(z -> setBoolean(grid, gridlines, new Location(Map.of(Axis.x, x, Axis.y, y, Axis.z, z)), input.state))));
    }

    enum State { on, off }
    enum Axis { x, y, z }
    record Input(State state, Map<Axis, Pair> bounds) {
        record Pair(int from, int to) {
            IntStream stream() {
                return IntStream.range(clamp(from), clamp(to+1));
            }
            private int clamp(int n) {
                return Math.max(-50, Math.min(n, 51));
            }
            IntStream matchingGridlines(Collection<Integer> gridlines) {
                return gridlines.stream()
                        .mapToInt(Integer::intValue)
                        .filter(i -> from <= i && i < to+1);
            }
        }

        static List<Input> parse(URL url) throws IOException {
            return Resources.readLines(url, StandardCharsets.UTF_8)
                    .stream()
                    .map(s -> {
                        String[] fields = s.split(" ");
                        String[] specs = fields[1].split(",");
                        Map<Axis, Pair> map = new HashMap<>();
                        for (String spec : specs) {
                            String[] parts = spec.split("=");
                            String[] coords = parts[1].split("\\.\\.");
                            map.put(Axis.valueOf(parts[0]), new Pair(Integer.parseInt(coords[0]), Integer.parseInt(coords[1])));
                        }
                        return new Input(State.valueOf(fields[0]), map);
                    })
                    .collect(Collectors.toList());
        }
    }

    record Point(Axis axis, Integer coordinate) {}
    static long size(Location location, Map<Point, Integer> nextBound) {
        return location.coordinates.entrySet().stream()
                .mapToLong(e -> nextBound.get(new Point(e.getKey(), e.getValue())) - e.getValue())
                .reduce((n, m) -> n*m)
                .getAsLong();
    }

    public static long part2(URL url) throws IOException {
        List<Input> inputs = Input.parse(url);
        SetMultimap<Axis, Integer> gridlinesTemp = HashMultimap.create();
        inputs.forEach(input -> input.bounds.entrySet().forEach(e -> {
            gridlinesTemp.put(e.getKey(), e.getValue().from);
            gridlinesTemp.put(e.getKey(), e.getValue().to+1);
        }));
        ListMultimap<Axis, Integer> gridlines = ArrayListMultimap.create(gridlinesTemp);
        boolean[] grid = createBoolean(gridlines);
        for (Input input : inputs) {
            applyCommand(grid, gridlines, input);
        }

        Map<Point, Integer> nextBound = new HashMap<>();
        for (Axis axis : gridlines.keys()) {
            List<Integer> marks = new ArrayList<>(gridlines.get(axis));
            Collections.sort(marks);
            for (int i = 0; i < marks.size() - 1; i++) {
                nextBound.put(new Point(axis, marks.get(i)), marks.get(i+1));
            }
        }
        return gridlines.get(Axis.x).stream()
                .flatMap(x -> gridlines.get(Axis.y).stream()
                        .flatMap(y -> gridlines.get(Axis.z).stream()
                                .map(z -> new Location(Map.of(Axis.x, x, Axis.y, y, Axis.z, z)))
                                .filter(l -> getBoolean(grid, gridlines, l))))
                .mapToLong(l -> size(l, nextBound))
                .sum();
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(39L, part1(Resources.getResource(getClass(), "22sample1.txt")));
        assertEquals(590784L, part1(Resources.getResource(getClass(), "22sample2.txt")));
        assertEquals(474140L, part1(Resources.getResource(getClass(), "22sample3.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(647076L, part1(Resources.getResource(getClass(), "22.txt")));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(2758514936282235L, part2(Resources.getResource(getClass(), "22sample3.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(1233304599156793L, part2(Resources.getResource(getClass(), "22.txt")));
    }
}
