import com.google.common.base.Preconditions;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day24 {
    public static long part1(URL url) throws IOException {
        List<Integer> numeralOrder = new ArrayList<>(IntStream.rangeClosed(1, 9).boxed().toList());
        Collections.reverse(numeralOrder);
        return go(url, numeralOrder);
    }

    public static long go(URL url, List<Integer> numeralOrder) throws IOException {
        List<Instruction> instructions = parse(url);
        List<CodeBlock> codeBlocks = optimize(instructions);
        State state1 = codeBlocks.get(0).run(State.initial(), 0L);
        return tryDigit(codeBlocks, new HashMap<>(), state1, 0L, 0, numeralOrder).get();
    }

    static Optional<Long> tryDigit(List<CodeBlock> blocks, Map<Integer, Set<State>> memo, State state, long value, int digits, List<Integer> numeralOrder) {
        memo.putIfAbsent(digits, new HashSet<>());
        if (memo.get(digits).add(state)) {
            if (digits >= 14) {
                return Optional.of(value).filter(ignored -> state.registers.get(Var.z) == 0);
            }
            for (int i : numeralOrder) {
                long nextValue = value * 10 + i;
                State nextState = blocks.get(digits + 1).run(state, i);
                Optional<Long> result = tryDigit(blocks, memo, nextState, nextValue, digits + 1, numeralOrder);
                if (result.isPresent()) {
                    return result;
                }
            }
        }
        return Optional.empty();
    }

    static List<CodeBlock> optimize(List<Instruction> instructions) {
        List<CodeBlock> result = new ArrayList<>();
        result.add(new CodeBlock(Optional.empty(), new ArrayList<>()));
        EnumMap<Var, Integer> registerToEpoch = new EnumMap<>(Map.of(Var.w, 0, Var.x, 0, Var.y, 0, Var.z, 0));
        for (Instruction instruction : instructions) {
            if (instruction.op == Op.inp) {
                result.add(new CodeBlock(Optional.of(instruction), new ArrayList<>()));
                registerToEpoch.put(instruction.var1, result.size() - 1);
            } else {
                int epoch = registerToEpoch.get(instruction.var1);
                if (instruction.var2.isPresent()) {
                    epoch = Math.max(epoch, registerToEpoch.get(instruction.var2.get()));
                    registerToEpoch.put(instruction.var1, epoch);
                }
                result.get(epoch).instructions.add(instruction);
            }
        }
        return result;
    }

    record CodeBlock(Optional<Instruction> inputInstruction, List<Instruction> instructions) {
        State run(State state, long input) {
            State state1;
            if (inputInstruction.isPresent()) {
                state1 = inputInstruction.get().update(state, Optional.of(input));
            } else {
                state1 = state;
            }
            for (Instruction instruction : instructions) {
                state1 = instruction.update(state1, Optional.empty());
            }
            return state1;
        }
    }

    enum Var { w, x, y, z }
    enum Op { inp, add, mul, div, mod, eql }

    record Instruction(Op op, Var var1, Optional<Long> immediate, Optional<Var> var2) {
        private Long resolve(State state, Optional<Long> immediate, Optional<Var> var2) {
            return immediate.or(() -> var2.map(state.registers::get)).get();
        }
        State update(State state, Optional<Long> input) {
            if (op == Op.inp) {
                Preconditions.checkArgument(immediate.isEmpty() && var2.isEmpty());
                Preconditions.checkArgument(input.isPresent());
            } else {
                Preconditions.checkArgument(immediate.isEmpty() ^ var2.isEmpty());
            }
            return switch (op) {
                case inp -> state.setRegister(var1, input.get());
                case add -> state.setRegister(var1, state.registers.get(var1) + resolve(state, immediate, var2));
                case mul -> state.setRegister(var1, state.registers.get(var1) * resolve(state, immediate, var2));
                case div -> state.setRegister(var1, state.registers.get(var1) / resolve(state, immediate, var2));
                case mod -> state.setRegister(var1, state.registers.get(var1) % resolve(state, immediate, var2));
                case eql -> state.setRegister(var1, state.registers.get(var1).equals(resolve(state, immediate, var2)) ? 1L : 0L);
            };
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(op);
            sb.append(" ");
            sb.append(var1);
            if (var2.isPresent()) {
                sb.append(" ");
                sb.append(var2.get());
            }
            if (immediate.isPresent()) {
                sb.append(" ");
                sb.append(immediate.get());
            }
            return sb.toString();
        }
    }

    record State(EnumMap<Var, Long> registers) {
        static State initial() {
            EnumMap<Var, Long> registers = new EnumMap<>(Var.class);
            for (Var var : Var.values()) {
                registers.put(var, 0L);
            }
            return new State(registers);
        }

        State setRegister(Var var, Long value) {
            EnumMap<Var, Long> registers = new EnumMap<Var, Long>(this.registers);
            registers.put(var, value);
            return new State(registers);
        }
    }

    static List<Instruction> parse(URL url) throws IOException {
        return parse(Resources.readLines(url, StandardCharsets.UTF_8));
    }

    static List<Instruction> parse(String lines) {
        return parse(Arrays.asList(lines.split("\n")));
    }

    static List<Instruction> parse(List<String> lines) {
        return lines
                .stream()
                .map(line -> {
                    String[] fields = line.split(" ");
                    Optional<Var> var2 = Optional.empty();
                    Optional<Long> immediate = Optional.empty();
                    if (fields.length > 2) {
                        try {
                            var2 = Optional.of(Var.valueOf(fields[2]));
                        } catch (IllegalArgumentException e) {
                            immediate = Optional.of(Long.valueOf(fields[2]));
                        }
                    }
                    return new Instruction(Op.valueOf(fields[0]), Var.valueOf(fields[1]), immediate, var2);
                })
                .toList();
    }

    public static long part2(URL url) throws IOException {
        List<Integer> numeralOrder = IntStream.rangeClosed(1, 9).boxed().toList();
        return go(url, numeralOrder);
    }

    @Test
    public void testOptimize() {
        List<Instruction> instructions = parse("""
                add z 1
                add x 1
                inp x
                add y 1
                add y x
                inp w
                eql y w
                """);
        assertEquals(List.of(
                new CodeBlock(Optional.empty(),
                        parse("""
                                add z 1
                                add x 1
                                add y 1""")),
                new CodeBlock(Optional.of(parse("inp x").get(0)), parse("add y x")),
                new CodeBlock(Optional.of(parse("inp w").get(0)), parse("eql y w"))),
                optimize(instructions));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(99919692496939L, part1(Resources.getResource(getClass(), "24.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(81914111161714L, part2(Resources.getResource(getClass(), "24.txt")));
    }
}
