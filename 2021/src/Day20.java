import com.google.common.base.Preconditions;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day20 {
    public static long part1(URL url) throws IOException {
        Input input = Input.parse(url);
        Image step1 = step(input.algorithm, input.image);
        Image step2 = step(input.algorithm, step1);
        Preconditions.checkState(step2.infinite == Pixel.dark);
        return step2.finite.size();
    }

    public static long part2(URL url) throws IOException {
        Input input = Input.parse(url);
        Image image = input.image;
        for (int i = 0; i < 50; i++) {
            image = step(input.algorithm, image);
        }
        Preconditions.checkState(image.infinite == Pixel.dark);
        return image.finite.size();
    }

    record Input(List<Pixel> algorithm, Image image) {
        static Input parse(URL url) throws IOException {
            List<String> lines = Resources.readLines(url, StandardCharsets.UTF_8);
            Preconditions.checkState(lines.get(1).equals(""));
            return new Input(lines.get(0).chars().mapToObj(Pixel::parse).toList(),
                    new Image(Pixel.dark,
                            IntStream.range(0, lines.size() - 2)
                                    .mapToObj(i -> IntStream.range(0, lines.get(i+2).length())
                                            .mapToObj(j -> Pixel.parse(lines.get(i+2).codePointAt(j)) == Pixel.light
                                                    ? Stream.of(new Pair(i, j))
                                                    : Stream.<Pair>of())
                                            .flatMap(Function.identity()))
                                    .flatMap(Function.identity())
                                    .collect(Collectors.toSet())));
        }
    }

    record Pair(long i, long j) {
        Stream<Pair> neighbors() {
            return Stream.of(
                    new Pair(i-1, j-1),
                    new Pair(i-1, j),
                    new Pair(i-1, j+1),
                    new Pair(i, j-1),
                    new Pair(i, j),
                    new Pair(i, j+1),
                    new Pair(i+1, j-1),
                    new Pair(i+1, j),
                    new Pair(i+1, j+1)
            );
        }
    }

    enum Pixel {
        dark, light;

        Pixel invert() {
            return switch(this) {
                case light -> dark;
                case dark -> light;
            };
        }

        static Pixel parse(int c) {
            return switch (c) {
                case '#' -> light;
                case '.' -> dark;
                default -> throw new IllegalArgumentException();
            };
        }

        @Override
        public String toString() {
            return switch (this) {
                case dark -> ".";
                case light -> "#";
            };
        }
    }

    record Image(Pixel infinite, Set<Pair> finite) {
        Pixel get(Pair p) {
            return finite.contains(p) ? infinite.invert() : infinite;
        }

        int window(Pair pair) {
            int result = 0;
            for (Pair p : ((Iterable<Pair>) pair.neighbors()::iterator)) {
                result *= 2;
                result += get(p) == Pixel.dark ? 0 : 1;
            }
            return result;
        }

        @Override
        public String toString() {
            LongSummaryStatistics iRange = finite.stream().mapToLong(Pair::i).summaryStatistics();
            LongSummaryStatistics jRange = finite.stream().mapToLong(Pair::j).summaryStatistics();
            return LongStream.rangeClosed(iRange.getMin(), iRange.getMax())
                    .mapToObj(i -> LongStream.rangeClosed(jRange.getMin(), jRange.getMax())
                            .mapToObj(j -> get(new Pair(i, j)))
                            .map(Pixel::toString)
                            .reduce("", String::concat) + "\n")
                    .reduce("", String::concat) + "\n";
        }
    }

    static Image step(List<Pixel> algorithm, Image image) {
        Pixel infinite = switch (image.infinite)  {
            case dark -> algorithm.get(0);
            case light -> algorithm.get(511);
        };

        Set<Pair> pixelsOfInterest = image.finite.stream()
                .flatMap(Pair::neighbors)
                .collect(Collectors.toSet());

        return new Image(
                infinite,
                pixelsOfInterest.stream().flatMap(p -> algorithm.get(image.window(p)) == infinite ? Stream.of() : Stream.of(p)).collect(Collectors.toSet())
        );
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(35L, part1(Resources.getResource(getClass(), "20sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(5081L, part1(Resources.getResource(getClass(), "20.txt")));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(3351L, part2(Resources.getResource(getClass(), "20sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(15088L, part2(Resources.getResource(getClass(), "20.txt")));
    }
}
