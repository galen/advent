import com.google.common.base.Preconditions;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.stream.*;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day25 {
    public static long part1(URL url) throws IOException {
        List<List<Square>> data = Square.parse(Resources.readLines(url, StandardCharsets.UTF_8));
        return run(data);
    }

    static long run(List<List<Square>> data) {
        for (long i = 1; ; i++) {
            System.out.println(show(data));
            List<List<Square>> data1 = step(data, Square.right);
            List<List<Square>> data2 = step(data1, Square.down);
            if (data2.equals(data)) {
                return i;
            }
            data = data2;
        }
    }

    static String show(List<List<Square>> data) {
        StringBuilder sb = new StringBuilder();
        data.forEach(line -> {
            line.forEach(cell -> {
                char c = cell == Square.empty ? '.' : cell == Square.right ? '>' : 'v';
                sb.append(c);
            });
            sb.append('\n');
        });
        return sb.toString();
    }

    static List<List<Square>> step(List<List<Square>> data, Square phase) {
        Preconditions.checkArgument(phase != Square.empty);
        List<List<Square>> result = IntStream.range(0, data.size())
                .mapToObj(i -> IntStream.range(0, data.get(i).size())
                        .mapToObj(ignored -> Square.empty)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());

        for (int i = 0; i < data.size(); i++) {
            for (int j = 0; j < data.get(i).size(); j++) {
                int i1 = (i + (phase == Square.down ? 1 : 0)) % data.size();
                int j1 = (j + (phase == Square.right ? 1 : 0)) % data.get(i).size();
                if (data.get(i).get(j) == phase) {
                    if (data.get(i1).get(j1) == Square.empty) {
                        result.get(i1).set(j1, phase);
                    } else {
                        result.get(i).set(j, phase);
                    }
                } else if (data.get(i).get(j) != Square.empty) {
                    result.get(i).set(j, data.get(i).get(j));
                }
            }
        }

        return result;
    }

    enum Square {
        right, down, empty;

        static Square parse(int c) {
            return switch (c) {
                case '>' -> Square.right;
                case 'v' -> Square.down;
                case '.' -> Square.empty;
                default -> throw new IllegalArgumentException();
            };
        }

        static List<Square> parse(String s) {
            return s.chars().mapToObj(Square::parse).toList();
        }

        static List<List<Square>> parse(List<String> lines) {
            return lines.stream().map(Square::parse).toList();
        }
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(58L, part1(Resources.getResource(getClass(), "25sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(453L, part1(Resources.getResource(getClass(), "25.txt")));
    }
}