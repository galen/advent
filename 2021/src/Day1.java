import com.google.common.collect.Streams;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class Day1 {
    public static long part1(URL url) throws IOException {
        List<Long> numbers = getNumbers(url);

        return countIncreasing(numbers);
    }

    private static long countIncreasing(List<Long> numbers) {
        return Streams.zip(numbers.stream(), numbers.subList(1, numbers.size()).stream(), (n1, n2) -> n1 < n2)
                .filter(x -> x)
                .count();
    }

    private static List<Long> getNumbers(URL url) throws IOException {
        return Resources.readLines(url, StandardCharsets.UTF_8)
                .stream()
                .map(Long::valueOf)
                .collect(Collectors.toList());
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(7, part1(Resources.getResource(Day1.class, "1sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(1692, part1(Resources.getResource(Day1.class, "1.txt")));
    }

    public static long part2(URL url) throws IOException {
        List<Long> numbers = getNumbers(url);

        var windows = Streams.zip(Streams.zip(numbers.stream(), numbers.subList(1, numbers.size()).stream(), Math::addExact),
                numbers.subList(2, numbers.size()).stream(), Math::addExact)
                .collect(Collectors.toList());

        return countIncreasing(windows);
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(5, part2(Resources.getResource(Day1.class, "1sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(1724, part2(Resources.getResource(Day1.class, "1.txt")));
    }
}