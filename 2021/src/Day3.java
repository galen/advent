import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day3 {
    record Table(int length, int[] zeros, int[] ones) {}

    public static int part1(URL url) throws IOException {
        List<String> lines = Resources.readLines(url, StandardCharsets.UTF_8);
        Table table = tabulate(lines);
        int gamma = 0;
        int epsilon = 0;
        for (int i = 0; i < table.length; i++) {
            gamma *= 2;
            epsilon *= 2;
            if (table.zeros()[i] > table.ones()[i]) {
                epsilon += 1;
            } else {
                gamma += 1;
            }
        }
        return gamma * epsilon;
    }

    private static Table tabulate(List<String> lines) {
        int length = lines.get(0).length();
        int[] zeros = new int[length];
        int[] ones = new int[length];
        for (String line : lines) {
            for (int i = 0; i < length; i++) {
                byte[] bytes = line.getBytes(StandardCharsets.UTF_8);
                if (bytes[i] == '0') {
                    zeros[i]++;
                } else {
                    ones[i]++;
                }
            }
        }
        return new Table(length, zeros, ones);
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(198, part1(Resources.getResource(getClass(), "3sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(2035764, part1(Resources.getResource(getClass(), "3.txt")));
    }

    public static int part2(URL url) throws IOException {
        List<String> lines = Resources.readLines(url, StandardCharsets.UTF_8);
        int oxygen = filter(lines, false);
        int co2 = filter(lines, true);
        return oxygen * co2;
    }

    static int filter(List<String> lines, boolean leastCommon) {
        for (int i = 0; i < lines.get(0).length(); i++) {
            if (lines.size() == 1) {
                break;
            }
            final int j = i;
            Table table = tabulate(lines);
            lines = lines.stream()
                    .filter(line -> line.getBytes(StandardCharsets.UTF_8)[j] ==
                            (leastCommon ^ (table.zeros[j] > table.ones[j]) ? '0' : '1'))
                    .collect(Collectors.toList());
        }
        return Integer.parseInt(lines.get(0), 2);
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(230, part2(Resources.getResource(getClass(), "3sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(2817661, part2(Resources.getResource(getClass(), "3.txt")));
    }
}
