import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day10 {
    public static long part1(URL url) throws IOException {
        return Resources.readLines(url, StandardCharsets.UTF_8)
                .stream()
                .mapToLong(Day10::parse)
                .sum();
    }

    sealed interface Result {
        record Syntax(int pos) implements Result {}
        record Error(int pos, int value) implements Result {}
    }

    static Map<Character, Character> pairs = Map.of(
            '(', ')',
            '[', ']',
            '{', '}',
            '<', '>'
    );

    static Map<Character, Integer> score = Map.of(
            ')', 3,
            ']', 57,
            '}', 1197,
            '>', 25137
    );

    private static int parse(String s) {
        int pos = 0;
        while (pos < s.length()) {
            Result result = parse(s, pos);
            if (result instanceof Result.Error e) {
                return e.value;
            } else if (result instanceof Result.Syntax rs) {
                if (pos == rs.pos) {
                    return score.get((char) s.codePointAt(pos));
                }
                pos = rs.pos;
            }
        }
        return 0;
    }

    private static Result parse(String s, int start) {
        char starting = (char) s.codePointAt(start);
        Character matching = pairs.get(starting);
        if (matching == null) {
            return new Result.Syntax(start);
        }
        Result step = null;
        int last = start + 1;
        while (step == null || step instanceof Result.Syntax rs && rs.pos > last) {
            if (step instanceof Result.Syntax rs) {
                last = rs.pos;
            }
            if (last >= s.length()) {
                // line is incomplete
                return new Result.Syntax(last);
            }
            step = parse(s, last);
        }
        return switch (step) {
            case Result.Syntax rs -> {
                char actual = (char) s.codePointAt(rs.pos);
                if (actual == matching) {
                    yield new Result.Syntax(rs.pos + 1);
                } else {
                    yield new Result.Error(rs.pos+1, score.get(actual));
                }
            }
            case Result.Error re -> re;
        };
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(26397L, part1(Resources.getResource(getClass(), "10sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(311949L, part1(Resources.getResource(getClass(), "10.txt")));
    }

    public static long part2(URL url) throws IOException {
        List<Long> scores = Resources.readLines(url, StandardCharsets.UTF_8)
                .stream()
                .map(Day10::complete)
                .filter(score -> score > 0)
                .sorted()
                .collect(Collectors.toList());
        return scores.get(scores.size() / 2);
    }

    sealed interface CompletionResult {
        record Syntax(int pos) implements CompletionResult {}
        record Error() implements CompletionResult {}
        record Completion(long score) implements CompletionResult {}
    }

    static Map<Character, Long> completionScore = Map.of(
            ')', 1L,
            ']', 2L,
            '}', 3L,
            '>', 4L
    );

    private static long complete(String s) {
        int pos = 0;
        while (pos < s.length()) {
            CompletionResult result = complete(s, pos);
            if (result instanceof CompletionResult.Error e) {
                return 0;
            } else if (result instanceof CompletionResult.Completion c) {
                return c.score;
            } else if (result instanceof CompletionResult.Syntax rs) {
                pos = rs.pos;
            }
        }
        return 0;
    }

    private static CompletionResult complete(String s, int start) {
        char starting = (char) s.codePointAt(start);
        Character matching = pairs.get(starting);
        if (matching == null) {
            return new CompletionResult.Syntax(start);
        }
        CompletionResult step = null;
        int last = start + 1;
        while (step == null || step instanceof CompletionResult.Syntax rs && rs.pos > last) {
            if (step instanceof CompletionResult.Syntax rs) {
                last = rs.pos;
            }
            if (last >= s.length()) {
                // line is incomplete
                return new CompletionResult.Completion(completionScore.get(matching));
            }
            step = complete(s, last);
        }
        return switch (step) {
            case CompletionResult.Syntax rs -> {
                char actual = (char) s.codePointAt(rs.pos);
                if (actual == matching) {
                    yield new CompletionResult.Syntax(rs.pos + 1);
                } else {
                    yield new CompletionResult.Error();
                }
            }
            case CompletionResult.Completion c -> new CompletionResult.Completion(5*c.score + completionScore.get(matching));
            case CompletionResult.Error re -> re;
        };
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(288957L, part2(Resources.getResource(getClass(), "10sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(3042730309L, part2(Resources.getResource(getClass(), "10.txt")));
    }
}
