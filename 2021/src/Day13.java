import com.google.common.base.Preconditions;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day13 {
    record Coordinates(int y, int x) {
        static Coordinates fromList(List<Integer> coordinates) {
            return new Coordinates(coordinates.get(1), coordinates.get(0));
        }

        enum AxisName {y, x}

        int get(AxisName axisName) {
            return switch (axisName) {
                case y -> y;
                case x -> x;
            };
        }

        Coordinates update(AxisName axisName, int value) {
            return switch (axisName) {
                case y -> new Coordinates(value, x);
                case x -> new Coordinates(y, value);
            };
        }
    }

    record FoldInstruction(Coordinates.AxisName axisName, int coordinate) {
    }

    record Input(List<Coordinates> coordinates, List<FoldInstruction> foldInstructions) {
    }

    public static long part1(URL url) throws IOException {
        Input input = parse(url);
        return performFold(input.coordinates, input.foldInstructions.get(0)).size();
    }

    static Input parse(URL url) throws IOException {
        List<String> lines = Resources.readLines(url, StandardCharsets.UTF_8);
        int division = lines.indexOf("");
        List<Coordinates> coordinates = lines.subList(0, division).stream()
                .map(line -> Coordinates.fromList(Arrays.stream(line.split(",")).map(Integer::parseInt).collect(Collectors.toList())))
                .collect(Collectors.toList());
        List<FoldInstruction> foldInstructions = new ArrayList<>();
        for (String line : lines.subList(division + 1, lines.size())) {
            String[] words = line.split(" ");
            String[] parts = words[2].split("=");
            foldInstructions.add(new FoldInstruction(Coordinates.AxisName.valueOf(parts[0]), Integer.parseInt(parts[1])));
        }
        return new Input(coordinates, foldInstructions);
    }

    static List<Coordinates> performFold(List<Coordinates> board, FoldInstruction foldInstruction) {
        Preconditions.checkState(board.stream().map(c -> c.get(foldInstruction.axisName)).filter(value -> value == foldInstruction.coordinate).findAny().isEmpty(),
                "fold line nonempty");
        return board.stream()
                .map(coordinates -> coordinates.update(foldInstruction.axisName, foldInstruction.coordinate - Math.abs(coordinates.get(foldInstruction.axisName) - foldInstruction.coordinate)))
                .distinct()
                .collect(Collectors.toList());
    }

    static List<Coordinates> performFolds(List<Coordinates> coordinates, List<FoldInstruction> foldInstructions) {
        List<Coordinates> result = coordinates;
        for (FoldInstruction foldInstruction : foldInstructions) {
            result = performFold(result, foldInstruction);
        }
        return result;
    }

    public static long part2(URL url) throws IOException {
        Input input = parse(url);
        List<Coordinates> result = performFolds(input.coordinates, input.foldInstructions);
        int maxY = result.stream().mapToInt(Coordinates::y).max().getAsInt();
        int maxX = result.stream().mapToInt(Coordinates::x).max().getAsInt();
        System.out.println(
                IntStream.rangeClosed(0, maxY).mapToObj(y ->
                                IntStream.rangeClosed(0, maxX).mapToObj(x ->
                                                result.contains(new Coordinates(y, x)) ? "#" : ".")
                                        .reduce(String::concat)
                                        .get() + "\n"
                        )
                        .reduce(String::concat)
                        .get()
        );
        return result.size();
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(17L, part1(Resources.getResource(getClass(), "13sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(695L, part1(Resources.getResource(getClass(), "13.txt")));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(16L, part2(Resources.getResource(getClass(), "13sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(89L, part2(Resources.getResource(getClass(), "13.txt")));
    }
}
