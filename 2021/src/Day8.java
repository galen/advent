import com.google.common.base.Enums;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;
import com.google.common.collect.*;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day8 {
    enum Letter { a, b, c, d, e, f, g }
    static EnumSet<Letter> parse(String word) {
        String[] letters = word.split("");
        return Arrays.stream(letters)
                .map(Letter::valueOf)
                .collect(Collectors.collectingAndThen(Collectors.toList(), EnumSet::copyOf));
    }

    record Line(List<EnumSet<Letter>> allSegments, List<EnumSet<Letter>> number) {}

    public static long part1(URL url) throws IOException {
        List<Line> lines = parse(url);

        return lines.stream()
                .flatMap(line -> line.number.stream())
                .filter(segments -> Set.of(2, 4, 3, 7).contains(segments.size()))
                .count();
    }

    private static List<Line> parse(URL url) throws IOException {
        return Resources.readLines(url, StandardCharsets.UTF_8)
                .stream()
                .map(Splitter.on(" ")::splitToList)
                .map(tokens -> {
                    int index = tokens.indexOf("|");
                    return new Line(tokens.subList(0, index).stream().map(Day8::parse).collect(Collectors.toList()),
                            tokens.subList(index + 1, tokens.size()).stream().map(Day8::parse).collect(Collectors.toList()));
                })
                .collect(Collectors.toList());
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(26L, part1(Resources.getResource(getClass(), "8sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(525L, part1(Resources.getResource(getClass(), "8.txt")));
    }

    static <T extends Enum<T>> EnumSet<T> difference(EnumSet<T> first, EnumSet<T> second) {
        EnumSet<T> result = EnumSet.copyOf(first);
        result.removeAll(second);
        return result;
    }

    static <K, V> V fetch(Multimap<K, V> map, K key) {
        return fetch(map, key, x -> true);
    }

    static <K, V> V fetch(Multimap<K, V> map, K key, Predicate<V> predicate) {
        V result = map.get(key).stream().filter(predicate).collect(MoreCollectors.onlyElement());
        map.remove(key, result);
        return result;
    }

    static Map<EnumSet<Letter>, Integer> solve(List<EnumSet<Letter>> allSegments) {
        Multimap<Integer, EnumSet<Letter>> byCount = HashMultimap.create();
        for (EnumSet<Letter> value : allSegments) {
            byCount.put(value.size(), value);
        }

        BiMap<Integer, EnumSet<Letter>> result = HashBiMap.create();
        result.put(1, fetch(byCount, 2));
        result.put(7, fetch(byCount, 3));
        result.put(4, fetch(byCount, 4));
        result.put(8, fetch(byCount, 7));

        result.put(6, fetch(byCount, 6, s -> difference(s, result.get(1)).size() == 5));
        result.put(9, fetch(byCount, 6, s -> difference(s, result.get(4)).size() == 2));
        result.put(0, fetch(byCount, 6));

        result.put(2, fetch(byCount, 5, s -> difference(s, result.get(4)).size() == 3));
        result.put(3, fetch(byCount, 5, s -> difference(s, result.get(1)).size() == 3));
        result.put(5, fetch(byCount, 5));

        return result.inverse();
    }

    static int output(Map<EnumSet<Letter>, Integer> solution, List<EnumSet<Letter>> number) {
        int result = 0;
        for (EnumSet<Letter> digit : number) {
            result *= 10;
            result += solution.get(digit);
        }
        return result;
    }

    public static int part2(URL url) throws IOException {
        return parse(url).stream().mapToInt(line -> output(solve(line.allSegments), line.number)).sum();
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(61229L, part2(Resources.getResource(getClass(), "8sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(1083859L, part2(Resources.getResource(getClass(), "8.txt")));
    }
}
