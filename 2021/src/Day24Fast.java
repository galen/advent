import java.util.*;
import java.util.function.BiConsumer;

public class Day24Fast implements Cloneable {
    static long startTime = System.currentTimeMillis();
    static List<BiConsumer<Day24Fast, Integer>> BLOCKS = List.of(
            Day24Fast::g1,
            Day24Fast::g2,
            Day24Fast::g3,
            Day24Fast::g4,
            Day24Fast::g5,
            Day24Fast::g6,
            Day24Fast::g7,
            Day24Fast::g8,
            Day24Fast::g9,
            Day24Fast::g10,
            Day24Fast::g11,
            Day24Fast::g12,
            Day24Fast::g13,
            Day24Fast::g14
    );

    int x = 0;
    int z = 0;

    public static void main(String[] args) throws Exception {
        Day24Fast machine = new Day24Fast();
        machine.f();
        System.out.println(machine.tryDigit(0, 0));
    }

    static Set<State> memo = new HashSet<>();
    record State(Day24Fast state, int digit) {}

    long tryDigit(long value, int digits) throws CloneNotSupportedException {
        if (x < 1 || x > 9) {
            x = 0;
        }
        State state = new State(this, digits);
        if (memo.contains(state)) {
            return -1;
        }
        if (digits == 4) {
            System.out.printf("%s: %s%n", value, System.currentTimeMillis() - startTime);
        }
        if (digits >= 14) {
            return z == 0 ? value : -1;
        }
//        for (int i = 9; i >= 1; i--) {
        for (int i = 1; i <= 9; i++) {
            long nextValue = value*10 + i;
            Day24Fast next = (Day24Fast) this.clone();
            BLOCKS.get(digits).accept(next, i);
            long result = next.tryDigit(nextValue, digits+1);
            if (result != -1) {
                return result;
            }
        }
        memo.add(state);
        return -1;
    }

    @Override
    public String toString() {
        return "%s,%s".formatted(x, z);
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof Day24Fast that && this.x == that.x && this.z == that.z;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, z);
    }

    void f() {
        x *= 0;
        x += z;
        x %= 26;
        z /= 1;
        x += 10;
    }

    void g1(int w) {
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        int y = 0;
        y += 25;
        y *= x;
        y += 1;
        z *= y;
        y = 0;
        y += w;
        y += 5;
        y *= x;
        z += y;
        x *= 0;
        x += z;
        x %= 26;
        z /= 1;
        x += 13;
    }

    void g2(int w) {
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        int y = 0;
        y += 25;
        y *= x;
        y += 1;
        z *= y;
        y = 0;
        y += w;
        y += 9;
        y *= x;
        z += y;
        x *= 0;
        x += z;
        x %= 26;
        z /= 1;
        x += 12;
    }

    void g3(int w) {
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        int y = 0;
        y += 25;
        y *= x;
        y += 1;
        z *= y;
        y = 0;
        y += w;
        y += 4;
        y *= x;
        z += y;
        x *= 0;
        x += z;
        x %= 26;
        z /= 26;
        x -= 12;
    }

    void g4(int w) {
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        int y = 0;
        y += 25;
        y *= x;
        y += 1;
        z *= y;
        y = 0;
        y += w;
        y += 4;
        y *= x;
        z += y;
        x *= 0;
        x += z;
        x %= 26;
        z /= 1;
        x += 11;
    }

    void g5(int w) {
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        int y = 0;
        y += 25;
        y *= x;
        y += 1;
        z *= y;
        y = 0;
        y += w;
        y += 10;
        y *= x;
        z += y;
        x *= 0;
        x += z;
        x %= 26;
        z /= 26;
        x -= 13;
    }

    void g6(int w) {
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        int y = 0;
        y += 25;
        y *= x;
        y += 1;
        z *= y;
        y = 0;
        y += w;
        y += 14;
        y *= x;
        z += y;
        x *= 0;
        x += z;
        x %= 26;
        z /= 26;
        x -= 9;
    }

    void g7(int w) {
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        int y = 0;
        y += 25;
        y *= x;
        y += 1;
        z *= y;
        y = 0;
        y += w;
        y += 14;
        y *= x;
        z += y;
        x *= 0;
        x += z;
        x %= 26;
        z /= 26;
        x -= 12;
    }

    void g8(int w) {
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        int y = 0;
        y += 25;
        y *= x;
        y += 1;
        z *= y;
        y = 0;
        y += w;
        y += 12;
        y *= x;
        z += y;
        x *= 0;
        x += z;
        x %= 26;
        z /= 1;
        x += 14;
    }

    void g9(int w) {
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        int y = 0;
        y += 25;
        y *= x;
        y += 1;
        z *= y;
        y = 0;
        y += w;
        y += 14;
        y *= x;
        z += y;
        x *= 0;
        x += z;
        x %= 26;
        z /= 26;
        x -= 9;
    }

    void g10(int w) {
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        int y = 0;
        y += 25;
        y *= x;
        y += 1;
        z *= y;
        y = 0;
        y += w;
        y += 14;
        y *= x;
        z += y;
        x *= 0;
        x += z;
        x %= 26;
        z /= 1;
        x += 15;
    }

    void g11(int w) {
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        int y = 0;
        y += 25;
        y *= x;
        y += 1;
        z *= y;
        y = 0;
        y += w;
        y += 5;
        y *= x;
        z += y;
        x *= 0;
        x += z;
        x %= 26;
        z /= 1;
        x += 11;
    }

    void g12(int w) {
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        int y = 0;
        y += 25;
        y *= x;
        y += 1;
        z *= y;
        y = 0;
        y += w;
        y += 10;
        y *= x;
        z += y;
        x *= 0;
        x += z;
        x %= 26;
        z /= 26;
        x -= 16;
    }

    void g13(int w) {
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        int y = 0;
        y += 25;
        y *= x;
        y += 1;
        z *= y;
        y = 0;
        y += w;
        y += 8;
        y *= x;
        z += y;
        x *= 0;
        x += z;
        x %= 26;
        z /= 26;
        x -= 2;
    }

    void g14(int w) {
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        int y = 0;
        y += 25;
        y *= x;
        y += 1;
        z *= y;
        y = 0;
        y += w;
        y += 15;
        y *= x;
        z += y;
    }
}
