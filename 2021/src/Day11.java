import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day11 {
    static List<List<Integer>> parse(URL url) throws IOException {
        return Resources.readLines(url, StandardCharsets.UTF_8).stream()
                .map(line -> Arrays.stream(line.split("")).map(Integer::parseInt).collect(Collectors.toList()))
                .collect(Collectors.toList());
    }

    public static long part1(URL url) throws IOException {
        List<List<Integer>> map = parse(url);
        AtomicLong flashCount = new AtomicLong();
        for (int i = 0; i < 100; i++) {
            increment(map);
            flash(map, flashCount);
            reset(map);
        }
        return flashCount.get();
    }

    record Coordinates(int row, int col) {}
    private static void flash(List<List<Integer>> map, AtomicLong flashCount) {
        boolean flashed;
        Set<Coordinates> seen = new HashSet<>();
        do {
            flashed = false;
            for (int row = 0; row < map.size(); row++) {
                for (int col = 0; col < map.get(row).size(); col++) {
                    if (map.get(row).get(col) > 9 && !seen.contains(new Coordinates(row, col))) {
                        flashed = true;
                        flashCount.incrementAndGet();
                        seen.add(new Coordinates(row, col));
                        for (int dRow = -1; dRow <= 1; dRow++) {
                            for (int dCol = -1; dCol <= 1; dCol++) {
                                int row1 = row + dRow;
                                int col1 = col + dCol;
                                if (!(dRow == 0 && dCol == 0)
                                        && row1 >= 0
                                        && row1 < map.size()
                                        && col1 >= 0
                                        && col1 < map.get(row1).size()) {
                                    map.get(row1).set(col1, 1 + map.get(row1).get(col1));
                                }
                            }
                        }
                    }
                }
            }
        } while (flashed);
    }

    private static void reset(List<List<Integer>> map) {
        for (int row = 0; row < map.size(); row++) {
            for (int col = 0; col < map.get(row).size(); col++) {
                if (map.get(row).get(col) > 9) {
                    map.get(row).set(col, 0);
                }
            }
        }
    }

    private static void increment(List<List<Integer>> map) {
        for (int row = 0; row < map.size(); row++) {
            for (int col = 0; col < map.get(row).size(); col++) {
                map.get(row).set(col, 1 + map.get(row).get(col));
            }
        }
    }

    public static long part2(URL url) throws IOException {
        List<List<Integer>> map = parse(url);
        for (int i = 1; ; i++) {
            increment(map);
            AtomicLong flashCount = new AtomicLong();
            flash(map, flashCount);
            if (flashCount.get() == map.size() * map.get(0).size()) {
                return i;
            }
            reset(map);
        }
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(1656L, part1(Resources.getResource(getClass(), "11sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(1675L, part1(Resources.getResource(getClass(), "11.txt")));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(195L, part2(Resources.getResource(getClass(), "11sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(515L, part2(Resources.getResource(getClass(), "11.txt")));
    }
}
