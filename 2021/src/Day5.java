import com.google.common.base.Splitter;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day5 {
    record Point(int x, int y) {}
    record Line(Point a, Point b) {}
    
    public static long part1(URL url) throws IOException {
        List<Line> lines = parse(url);
        Point bounds = bounds(lines);
        List<List<Integer>> marks = createMarks(bounds);
        mark(marks, vertical(lines));
        mark(marks, horizontal(lines));
        return marks.stream()
                .mapToLong(row -> row.stream().filter(x -> x >= 2).count())
                .sum();
    }

    private static void mark(List<List<Integer>> marks, List<Line> lines) {
        for (Line line : lines) {
            for (Point point : pointsBetween(line.a, line.b)) {
                marks.get(point.x).set(point.y, marks.get(point.x).get(point.y) + 1);
            }
        }
    }

    private static List<List<Integer>> createMarks(Point bounds) {
        return IntStream.rangeClosed(0, bounds.x)
                .mapToObj(i -> IntStream.rangeClosed(0, bounds.y)
                        .mapToObj(j -> 0)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
    }

    private static Point bounds(List<Line> lines) {
        return lines.stream()
                .flatMap(p -> Stream.of(p.a, p.b))
                .reduce(new Point(0, 0), (p, q) -> new Point(Math.max(p.x, q.x), Math.max(p.y, q.y)), (p, q) -> new Point(Math.max(p.x, q.x), Math.max(p.y, q.y)));
    }

    private static List<Point> pointsBetween(Point a, Point b) {
        if (a.x == b.x) {
            return IntStream.rangeClosed(a.y, b.y)
                    .mapToObj(y -> new Point(a.x, y))
                    .collect(Collectors.toList());
        } else if (a.y == b.y) {
            return IntStream.rangeClosed(a.x, b.x)
                    .mapToObj(x -> new Point(x, a.y))
                    .collect(Collectors.toList());
        } else if (a.x < b.x) {
            return IntStream.rangeClosed(0, b.x - a.x)
                    .mapToObj(i -> new Point(a.x + i, a.y + i))
                    .collect(Collectors.toList());
        } else {
            return IntStream.rangeClosed(0, a.x - b.x)
                    .mapToObj(i -> new Point(a.x - i, a.y + i))
                    .collect(Collectors.toList());
        }
    }

    private static List<Line> vertical(List<Line> lines) {
        return lines.stream()
                .filter(line -> line.a.x == line.b.x)
                .map(line -> line.a.y < line.b.y ? line : new Line(line.b, line.a))
                .collect(Collectors.toList());
    }

    private static List<Line> horizontal(List<Line> lines) {
        return lines.stream()
                .filter(line -> line.a.y == line.b.y)
                .map(line -> line.a.x < line.b.x ? line : new Line(line.b, line.a))
                .collect(Collectors.toList());
    }

    private static List<Line> parse(URL url) throws IOException {
        return Resources.readLines(url, StandardCharsets.UTF_8)
                .stream()
                .map(s -> {
                     List<Point> pointList = Splitter.on(" -> ").splitToStream(s)
                            .map(rawPair -> {
                                List<Integer> pair = Splitter.on(",").splitToStream(rawPair)
                                        .map(Integer::parseInt)
                                        .collect(Collectors.toList());
                                return new Point(pair.get(0), pair.get(1));
                            })
                            .collect(Collectors.toList());
                     return new Line(pointList.get(0), pointList.get(1));
                })
                .collect(Collectors.toList());
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(5L, part1(Resources.getResource(getClass(), "5sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(8350L, part1(Resources.getResource(getClass(), "5.txt")));
    }

    public static long part2(URL url) throws IOException {
        List<Line> lines = parse(url);
        Point bounds = bounds(lines);
        List<List<Integer>> marks = createMarks(bounds);
        mark(marks, vertical(lines));
        mark(marks, horizontal(lines));
        mark(marks, diagonal(lines));
        return marks.stream()
                .mapToLong(row -> row.stream().filter(x -> x >= 2).count())
                .sum();
    }

    private static List<Line> diagonal(List<Line> lines) {
        return lines.stream()
                .filter(line -> line.a.x - line.b.x == line.a.y - line.b.y
                    || line.a.x - line.b.x == line.b.y - line.a.y)
                .map(line -> line.a.y < line.b.y ? line : new Line(line.b, line.a))
                .collect(Collectors.toList());
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(12L, part2(Resources.getResource(getClass(), "5sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(19374L, part2(Resources.getResource(getClass(), "5.txt")));
    }
}
