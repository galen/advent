import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day2 {
    enum Op {forward, down, up}

    record Command(Op op, int size) {}

    public static List<Command> parse(URL url) throws IOException {
        return Resources.readLines(url, StandardCharsets.UTF_8)
                .stream()
                .map(s -> {
                    String[] fields = s.split(" ");
                    return new Command(Op.valueOf(fields[0]), Integer.parseInt(fields[1]));
                })
                .collect(Collectors.toList());
    }

    public static <T, R> R foldLeft(Collection<T> collection, R identity, BiFunction<R, T, R> combiner) {
        R result = identity;
        for (T item : collection) {
            result = combiner.apply(result, item);
        }
        return result;
    }

    record State(int horizontal, int depth) {}

    public static int part1(URL url) throws IOException {
        State result = foldLeft(parse(url), new State(0, 0), (state, command) -> switch (command.op()) {
            case forward -> new State(state.horizontal() + command.size(), state.depth());
            case down -> new State(state.horizontal(), state.depth() + command.size());
            case up -> new State(state.horizontal(), state.depth() - command.size());
        });
        return result.depth() * result.horizontal();
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(150, part1(Resources.getResource(getClass(), "2sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(1636725, part1(Resources.getResource(getClass(), "2.txt")));
    }

    record AimState(int aim, int horizontal, int depth) {}

    public static int part2(URL url) throws IOException {
        AimState result = foldLeft(parse(url), new AimState(0, 0, 0), (state, command) -> switch (command.op()) {
            case forward -> new AimState(state.aim(), state.horizontal() + command.size(), state.depth() + state.aim() * command.size());
            case down -> new AimState(state.aim() + command.size(), state.horizontal(), state.depth());
            case up -> new AimState(state.aim() - command.size(), state.horizontal(), state.depth());
        });
        return result.depth() * result.horizontal();
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(900, part2(Resources.getResource(getClass(), "2sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(1872757425, part2(Resources.getResource(getClass(), "2.txt")));
    }
}
