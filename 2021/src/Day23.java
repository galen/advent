import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day23 {
    enum Kind {
        A(1, 2), B(10, 4), C(100, 6), D(1000, 8), empty(0, -1);
        final int energy;
        final int position;
        Kind(int energy, int position) {
            this.energy = energy;
            this.position = position;
        }
        static final List<Kind> pods = List.of(A, B, C, D);
    }

    record Burrow(List<Kind> hall, Map<Kind, List<Kind>> rooms) {
        static Burrow fromRooms(List<Kind> aResidents, List<Kind> bResidents, List<Kind> cResidents, List<Kind> dResidents) {
            EnumMap<Kind, List<Kind>> rooms = new EnumMap<>(Kind.class);
            rooms.put(Kind.A, aResidents);
            rooms.put(Kind.B, bResidents);
            rooms.put(Kind.C, cResidents);
            rooms.put(Kind.D, dResidents);
            return new Burrow(IntStream.range(0, 11).mapToObj(ignored -> Kind.empty).toList(), rooms);
        }
    }

    static int search(Burrow burrow) {
        PriorityQueue<State> queue = new PriorityQueue<>(Comparator.comparingInt(State::energy));
        HashSet<Burrow> seen = new HashSet<>();
        queue.add(new State(burrow, 0));
        while (!queue.isEmpty()) {
            State old = queue.poll();
            if (seen.add(old.burrow)) {
                if (done(old.burrow)) {
                    return old.energy;
                }
                queue.addAll(step(old));
            }
        }
        throw new IllegalStateException("unsolvable");
    }

    static boolean done(Burrow burrow) {
        for (Kind kind : Kind.pods) {
            if (!burrow.rooms.get(kind).stream().allMatch(kind::equals)) {
                return false;
            }
        }
        return true;
    }

    record State(Burrow burrow, int energy) {}

    static List<State> step(State state) {
        List<State> nextSteps = new ArrayList<>();
        for (Kind home : Kind.pods) {
            List<Kind> room = state.burrow.rooms.get(home);
            int maxDepth = room.size();
            while (maxDepth > 0 && room.get(maxDepth-1).equals(home)) {
                maxDepth--;
            }
            for (int roomPosition = 0; roomPosition < maxDepth; roomPosition++) {
                if (!room.get(roomPosition).equals(Kind.empty)) {
                    for (Integer destination : validDestinationsFromRoom(state.burrow, home)) {
                        nextSteps.add(moveFromRoom(state, home, destination, roomPosition));
                    }
                    break;
                }
            }
        }
        for (int hallPosition = 0; hallPosition < 11; hallPosition++) {
            tryMoveToDestination(state, hallPosition).ifPresent(nextSteps::add);
        }
        return nextSteps;
    }

    static Set<Integer> prohibited = Kind.pods.stream().map(kind -> kind.position).collect(Collectors.toSet());

    static List<Integer> validDestinationsFromRoom(Burrow burrow, Kind room) {
        int lowerBound = IntStream.rangeClosed(0, room.position)
                .filter(pos -> !burrow.hall.get(pos).equals(Kind.empty))
                .max()
                .orElse(-1);
        int upperBound = IntStream.rangeClosed(room.position, 10)
                .filter(pos -> !burrow.hall.get(pos).equals(Kind.empty))
                .min()
                .orElse(11);
        return IntStream.rangeClosed(lowerBound+1, upperBound-1)
                .filter(i -> !prohibited.contains(i))
                .boxed()
                .sorted(Comparator.comparingInt(i -> Math.abs(i - room.position)))
                .toList();
    }

    static State moveFromRoom(State state, Kind homeKind, int destination, int roomPosition) {
        Map<Kind, List<Kind>> rooms = new EnumMap<>(state.burrow.rooms);
        List<Kind> newRoom = new ArrayList<>(state.burrow.rooms.get(homeKind));
        newRoom.set(roomPosition, Kind.empty);
        rooms.put(homeKind, newRoom);
        List<Kind> newHall = new ArrayList<>(state.burrow.hall);
        Kind movingKind = state.burrow.rooms.get(homeKind).get(roomPosition);
        newHall.set(destination, movingKind);
        int distance = Math.abs(homeKind.position - destination) + 1 + roomPosition;
        return new State(new Burrow(newHall, rooms), state.energy + distance * movingKind.energy);
    }

    static Optional<State> tryMoveToDestination(State state, int position) {
        Kind occupant = state.burrow.hall.get(position);
        if (occupant.equals(Kind.empty)) {
            return Optional.empty();
        }
        int pos1, pos2;
        if (occupant.position < position) {
            pos1 = occupant.position;
            pos2 = position - 1;
        } else {
            pos1 = position + 1;
            pos2 = occupant.position;
        }
        if (IntStream.rangeClosed(pos1, pos2).anyMatch(p -> !state.burrow.hall.get(p).equals(Kind.empty))) {
            return Optional.empty();
        }
        List<Kind> homeRoom = state.burrow.rooms.get(occupant);
        int roomPosition;
        for (roomPosition = homeRoom.size() - 1; roomPosition >= 0; roomPosition--) {
            if (!homeRoom.get(roomPosition).equals(occupant)) {
                break;
            }
        }
        if (roomPosition < 0) {
            return Optional.empty();
        }
        for (int i = roomPosition; i >= 0; i--) {
            if (!homeRoom.get(roomPosition).equals(Kind.empty)) {
                return Optional.empty();
            }
        }
        Map<Kind, List<Kind>> rooms = new EnumMap<>(state.burrow.rooms);
        List<Kind> newRoom = new ArrayList<>(state.burrow.rooms.get(occupant));
        newRoom.set(roomPosition, occupant);
        rooms.put(occupant, newRoom);
        List<Kind> newHall = new ArrayList<>(state.burrow.hall);
        newHall.set(position, Kind.empty);
        int distance = Math.abs(position - occupant.position) + 1 + roomPosition;
        return Optional.of(new State(
                new Burrow(newHall, rooms),
                state.energy + distance * occupant.energy
        ));
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(12521, search(Burrow.fromRooms(List.of(Kind.B, Kind.A), List.of(Kind.C, Kind.D), List.of(Kind.B, Kind.C), List.of(Kind.D, Kind.A))));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(11516, search(Burrow.fromRooms(List.of(Kind.C, Kind.B), List.of(Kind.A, Kind.A), List.of(Kind.B, Kind.D), List.of(Kind.D, Kind.C))));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(44169, search(Burrow.fromRooms(List.of(Kind.B, Kind.D, Kind.D, Kind.A), List.of(Kind.C, Kind.C, Kind.B, Kind.D), List.of(Kind.B, Kind.B, Kind.A, Kind.C), List.of(Kind.D, Kind.A, Kind.C, Kind.A))));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(40272, search(Burrow.fromRooms(List.of(Kind.C, Kind.D, Kind.D, Kind.B), List.of(Kind.A, Kind.C, Kind.B, Kind.A), List.of(Kind.B, Kind.B, Kind.A, Kind.D), List.of(Kind.D, Kind.A, Kind.C, Kind.C))));
    }
}
