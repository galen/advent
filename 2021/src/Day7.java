import com.google.common.base.Splitter;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day7 {
    public static long part1(URL url) throws IOException {
        List<Long> positions = parseNumbers(url);

        Collections.sort(positions);
        long position = positions.get(positions.size() / 2);
        return positions.stream()
                .mapToLong(p -> Math.abs(p - position))
                .sum();
    }

    private static List<Long> parseNumbers(URL url) throws IOException {
        return Resources.readLines(url, StandardCharsets.UTF_8)
                .stream()
                .flatMap(Splitter.on(",")::splitToStream)
                .map(Long::parseLong)
                .collect(Collectors.toList());
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(37, part1(Resources.getResource(getClass(), "7sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(355764, part1(Resources.getResource(getClass(), "7.txt")));
    }

    public static long part2(URL url) throws IOException {
        List<Long> positions = parseNumbers(url);
        double average = positions.stream().mapToLong(x -> x).summaryStatistics().getAverage();
        return LongStream.of((long) average, ((long) average) + 1)
                .map(position -> positions.stream()
                    .mapToLong(p -> Math.abs(p - position))
                    .map(x -> x*(x+1)/2)
                    .sum())
                .min()
                .getAsLong();
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(168, part2(Resources.getResource(getClass(), "7sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(99634572, part2(Resources.getResource(getClass(), "7.txt")));
    }
}