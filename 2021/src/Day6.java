import com.google.common.base.Splitter;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day6 {
    public static long part1(URL url) throws IOException {
        return calculate(url, 80);
    }

    private static long calculate(URL url, int days) throws IOException {
        List<Integer> ages = Resources.readLines(url, StandardCharsets.UTF_8)
                .stream()
                .flatMap(Splitter.on(",")::splitToStream)
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        // Representation: (day + age) % 9 is the index
        long[] state = new long[9];
        for (int age : ages) {
            state[age] += 1;
        }

        for (int day = 0; day < days; day++) {
            state[(day + 7) % 9] += state[day % 9];
        }

        return LongStream.of(state).sum();
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(5934L, part1(Resources.getResource(getClass(), "6sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(363101L, part1(Resources.getResource(getClass(), "6.txt")));
    }

    public static long part2(URL url) throws IOException {
        return calculate(url, 256);
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(26984457539L, part2(Resources.getResource(getClass(), "6sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(1644286074024L, part2(Resources.getResource(getClass(), "6.txt")));
    }

}
