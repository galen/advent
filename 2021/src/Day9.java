import com.google.common.collect.Multimap;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day9 {
    public static long part1(URL url) throws IOException {
        List<List<Integer>> map = parse(url);
        return IntStream.range(0, map.size())
                .map(row -> IntStream.range(0, map.get(row).size())
                        .filter(col -> isLowPoint(map, row, col))
                        .map(col -> map.get(row).get(col) + 1)
                        .sum())
                .sum();
    }

    static List<List<Integer>> parse(URL url) throws IOException {
        return Resources.readLines(url, StandardCharsets.UTF_8).stream()
                .map(line -> Arrays.stream(line.split("")).map(Integer::parseInt).collect(Collectors.toList()))
                .collect(Collectors.toList());
    }

    static boolean isLowPoint(List<List<Integer>> map, int row, int col) {
        return (row == 0 || map.get(row - 1).get(col) > map.get(row).get(col))
                && (col == 0 || map.get(row).get(col - 1) > map.get(row).get(col))
                && (row == map.size() - 1 || map.get(row + 1).get(col) > map.get(row).get(col))
                && (col == map.get(0).size() - 1 || map.get(row).get(col + 1) > map.get(row).get(col));
    }

    record Coordinates(int row, int col) {}

    public static long part2(URL url) throws IOException {
        List<List<Integer>> map = parse(url);
        Map<Coordinates, Coordinates> basins = new HashMap<>();
        List<Coordinates> queue = IntStream.range(0, map.size())
                .mapToObj(row -> IntStream.range(0, map.get(row).size())
                        .filter(col -> isLowPoint(map, row, col))
                        .mapToObj(col -> new Coordinates(row, col)))
                .flatMap(Function.identity())
                .collect(Collectors.toList());
        for (Coordinates point : queue) {
            basins.put(point, point);
        }
        while (!queue.isEmpty()) {
            Coordinates point = queue.remove(0);
            // for each adjacent that is not 9: set its basin to this point's. add to queue
            for (Coordinates adjacency : adjacent(map, point)) {
                if (map.get(adjacency.row).get(adjacency.col) != 9 && !basins.containsKey(adjacency)) {
                    basins.put(adjacency, basins.get(point));
                    queue.add(adjacency);
                }
            }
        }
        Map<Coordinates, Integer> counts = new HashMap<>();
        for (Map.Entry<Coordinates, Coordinates> entry : basins.entrySet()) {
            counts.merge(entry.getValue(), 1, Integer::sum);
        }
        return counts.values().stream()
                .sorted(Comparator.<Integer>naturalOrder().reversed())
                .limit(3)
                .reduce(1, (x,y) -> x*y);
    }

    private static List<Coordinates> adjacent(List<List<Integer>> map, Coordinates point) {
        List<Coordinates> result = new ArrayList<>();
        if (point.row > 0) {
            result.add(new Coordinates(point.row - 1, point.col));
        }
        if (point.col > 0) {
            result.add(new Coordinates(point.row, point.col - 1));
        }
        if (point.row < map.size() - 1) {
            result.add(new Coordinates(point.row + 1, point.col));
        }
        if (point.col < map.get(point.row).size() - 1) {
            result.add(new Coordinates(point.row, point.col + 1));
        }
        return result;
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(15L, part1(Resources.getResource(getClass(), "9sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(500L, part1(Resources.getResource(getClass(), "9.txt")));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(1134L, part2(Resources.getResource(getClass(), "9sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(970200L, part2(Resources.getResource(getClass(), "9.txt")));
    }
}
