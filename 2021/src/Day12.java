import com.google.common.base.Splitter;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day12 {
    public static long part1(URL url) throws IOException {
        Multimap<String, String> edges = getEdges(url);
        return paths(edges, List.of("start")).count();
    }

    private static Multimap<String, String> getEdges(URL url) throws IOException {
        Multimap<String, String> edges = HashMultimap.create();
        for (String line : Resources.readLines(url, StandardCharsets.UTF_8)) {
            List<String> points = Splitter.on("-").splitToList(line);
            edges.put(points.get(0), points.get(1));
            edges.put(points.get(1), points.get(0));
        }
        return edges;
    }

    private static Stream<List<String>> paths(Multimap<String, String> edges, List<String> path) {
        if (path.get(path.size() - 1).equals("end")) {
            return Stream.of(path);
        }
        return edges.get(path.get(path.size() - 1)).stream()
                .flatMap(step -> {
                    if (path.contains(step) && step.toLowerCase().equals(step)) {
                        return Stream.of();
                    }
                    return paths(edges, ImmutableList.<String>builder().addAll(path).add(step).build());
                });
    }

    public static long part2(URL url) throws IOException {
        Multimap<String, String> edges = getEdges(url);
        return revisedPaths(edges, List.of("start")).count();
    }

    private static Stream<List<String>> revisedPaths(Multimap<String, String> edges, List<String> path) {
        if (path.get(path.size() - 1).equals("end")) {
            return Stream.of(path);
        }
        return edges.get(path.get(path.size() - 1)).stream()
                .flatMap(step -> {
                    if (path.contains(step) && restricted(step, path)) {
                        return Stream.of();
                    }
                    return revisedPaths(edges, ImmutableList.<String>builder().addAll(path).add(step).build());
                });
    }

    private static boolean restricted(String step, List<String> path) {
        List<String> visitedSmall = path.stream()
                .filter(s -> s.toLowerCase().equals(s))
                .collect(Collectors.toList());
        return step.toLowerCase().equals(step)
                && visitedSmall.stream().distinct().count() != visitedSmall.size()
                || step.equals("start")
                || step.equals("end");
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(10L, part1(Resources.getResource(getClass(), "12sample1.txt")));
        assertEquals(19L, part1(Resources.getResource(getClass(), "12sample2.txt")));
        assertEquals(226L, part1(Resources.getResource(getClass(), "12sample3.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(4773L, part1(Resources.getResource(getClass(), "12.txt")));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(36L, part2(Resources.getResource(getClass(), "12sample1.txt")));
        assertEquals(103L, part2(Resources.getResource(getClass(), "12sample2.txt")));
        assertEquals(3509L, part2(Resources.getResource(getClass(), "12sample3.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(116985L, part2(Resources.getResource(getClass(), "12.txt")));
    }
}
