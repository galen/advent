import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Day4 {
    public static int part1(URL url) throws IOException {
        Data data = parse(Resources.readLines(url, StandardCharsets.UTF_8));
        State state = createState(data);
        for (int i = 0; !hasWinner(state); i++) {
            state = updateState(state, i);
        }
        return score(state, findWinners(state).get(0));
    }

    record Data(List<Integer> draws, List<List<List<Integer>>> boards) {}

    static Data parse(List<String> lines) {
        List<Integer> draws = Arrays.stream(lines.get(0).split(","))
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        List<List<List<Integer>>> boards = new ArrayList<>();
        for (int i = 2; i < lines.size(); i += 6) {
            boards.add(lines.subList(i, i + 5)
                    .stream()
                    .map(line -> Arrays.stream(line.split(" +")).filter(s -> !s.isEmpty()).map(Integer::parseInt).collect(Collectors.toList()))
                    .collect(Collectors.toList()));
        }

        return new Data(draws, boards);
    }

    record State(List<Integer> draws, Integer lastDrawn, List<List<List<Integer>>> boards,
                 List<List<List<Boolean>>> marks) {}

    static State createState(Data data) {
        return new State(data.draws, null, data.boards, data.boards
                .stream()
                .map(board -> board.stream()
                        .map(row -> row.stream()
                                .map(cell -> false)
                                .collect(Collectors.toList()))
                        .collect(Collectors.toList()))
                .collect(Collectors.toList()));
    }

    static boolean hasWinner(State state) {
        return findWinners(state).size() > 0;
    }

    static List<Integer> findWinners(State state) {
        List<Integer> winners = new ArrayList<>();
        for (int board = 0; board < state.boards.size(); board++) {
            for (int row = 0; row < state.boards.get(board).size(); row++) {
                if (state.marks.get(board).get(row).stream().allMatch(x -> x)) {
                    winners.add(board);
                }
            }
            for (int col = 0; col < state.boards.get(board).get(0).size(); col++) {
                final int col1 = col;
                if (state.marks.get(board).stream().allMatch(row -> row.get(col1))) {
                    winners.add(board);
                }
            }
        }
        return winners;
    }

    static State updateState(State state, int i) {
        int lastDrawn = state.draws.get(i);
        for (int board = 0; board < state.boards.size(); board++) {
            for (int row = 0; row < state.boards.get(board).size(); row++) {
                for (int col = 0; col < state.boards.get(board).get(row).size(); col++) {
                    if (state.boards.get(board).get(row).get(col).equals(lastDrawn)) {
                        state.marks.get(board).get(row).set(col, true);
                    }
                }
            }
        }
        return new State(state.draws, lastDrawn, state.boards, state.marks);
    }

    static int score(State state, int board) {
        int sum = 0;
        for (int row = 0; row < state.boards.get(board).size(); row++) {
            for (int col = 0; col < state.boards.get(board).get(row).size(); col++) {
                if (!state.marks.get(board).get(row).get(col)) {
                    sum += state.boards.get(board).get(row).get(col);
                }
            }
        }
        return state.lastDrawn * sum;
    }

    @Test
    public void test1Sample() throws Exception {
        assertEquals(4512, part1(Resources.getResource(getClass(), "4sample.txt")));
    }

    @Test
    public void test1() throws Exception {
        assertEquals(29440, part1(Resources.getResource(getClass(), "4.txt")));
    }

    static int part2(URL url) throws IOException {
        Data data = parse(Resources.readLines(url, StandardCharsets.UTF_8));
        State state = createState(data);
        ImmutableSet.Builder<Integer> winners = ImmutableSet.builder();
        for (int i = 0; i < state.draws.size() && winners.build().size() < state.boards.size(); i++) {
            state = updateState(state, i);
            winners.addAll(findWinners(state));
        }
        ImmutableList<Integer> winnerList = winners.build().asList();
        return score(state, winnerList.get(winnerList.size() - 1));
    }

    @Test
    public void test2Sample() throws Exception {
        assertEquals(1924, part2(Resources.getResource(getClass(), "4sample.txt")));
    }

    @Test
    public void test2() throws Exception {
        assertEquals(13884, part2(Resources.getResource(getClass(), "4.txt")));
    }
}
