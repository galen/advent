with import <nixpkgs> {};

let eggs = import ./eggs.nix { inherit pkgs stdenv; };
in

stdenv.mkDerivation {
  name = "advent";
  buildInputs = [
    fswatch
    chicken
    eggs.srfi-1
    eggs.srfi-14
    eggs.srfi-63
    eggs.srfi-69
    eggs.srfi-151
    eggs.r7rs
    eggs.math
    eggs.amb
    eggs.comparse
  ];
}
