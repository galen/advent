{ pkgs, stdenv }:
rec {
  inherit (pkgs) eggDerivation fetchegg;

  amb = eggDerivation {
    name = "amb-3.0.3";

    src = fetchegg {
      name = "amb";
      version = "3.0.3";
      sha256 = "0zp98cviwafr5hkb7ayc0g1y2kp21vq7f593vqrsyf51p1yfpf0g";
    };

    buildInputs = [
      srfi-1
    ];
  };

  comparse = eggDerivation {
    name = "comparse-3";

    src = fetchegg {
      name = "comparse";
      version = "3";
      sha256 = "0xc6sa1690h4mfbgv82pmcildyk8y2cgfma8ih2gjkjwbm28p4c2";
    };

    buildInputs = [
      lazy-seq
      trie
      matchable
      srfi-1
      srfi-13
      srfi-14
      srfi-69
    ];
  };

  iset = eggDerivation {
    name = "iset-2.2";

    src = fetchegg {
      name = "iset";
      version = "2.2";
      sha256 = "0yfkcd07cw6xnnqfbbvjy81x0vc54k40vdjp2m7gwxx64is6m3rz";
    };

    buildInputs = [
      
    ];
  };

  lazy-seq = eggDerivation {
    name = "lazy-seq-2";

    src = fetchegg {
      name = "lazy-seq";
      version = "2";
      sha256 = "07d0v97r49f49ic2y73jyqcqj67z5zgaifykafd9fclxraff4s3s";
    };

    buildInputs = [
      srfi-1
    ];
  };

  matchable = eggDerivation {
    name = "matchable-1.1";

    src = fetchegg {
      name = "matchable";
      version = "1.1";
      sha256 = "084hm5dvbvgnpb32ispkp3hjili8z02hamln860r99jx68jx6j2v";
    };

    buildInputs = [
      
    ];
  };

  math = eggDerivation {
    name = "math-0.1.0";

    src = fetchegg {
      name = "math";
      version = "0.1.0";
      sha256 = "11c8ljayrcjbl39s7rdqzzqkkb8hvka508hnqc7qv9bmnv2qsbmx";
    };

    buildInputs = [
      srfi-1
      r6rs-bytevectors
      miscmacros
    ];
  };

  miscmacros = eggDerivation {
    name = "miscmacros-1.0";

    src = fetchegg {
      name = "miscmacros";
      version = "1.0";
      sha256 = "0n2ia4ib4f18hcbkm5byph07ncyx61pcpfp2qr5zijf8ykp8nbvr";
    };

    buildInputs = [
      
    ];
  };

  r6rs-bytevectors = eggDerivation {
    name = "r6rs-bytevectors-0.1.7";

    src = fetchegg {
      name = "r6rs-bytevectors";
      version = "0.1.7";
      sha256 = "1pm33cmzfbz3g9nmh038a8dsb2f7d40h36sdnvd43wvi7dmv6akm";
    };

    buildInputs = [
      utf8
    ];
  };

  r7rs = eggDerivation {
    name = "r7rs-1.0.3";

    src = fetchegg {
      name = "r7rs";
      version = "1.0.3";
      sha256 = "183f55k0kgfgfpawkivqmdyzilmjx3bsd0wamv5lfdmwiy1rlvii";
    };

    buildInputs = [
      matchable
      srfi-1
      srfi-13
    ];
  };

  records = eggDerivation {
    name = "records-1.4";

    src = fetchegg {
      name = "records";
      version = "1.4";
      sha256 = "187fsi0kb50vwc64fn4ijb5q3jfyyn95bn9v6hqp4xfjfic9fll9";
    };

    buildInputs = [
      srfi-1
    ];
  };

  regex = eggDerivation {
    name = "regex-2.0";

    src = fetchegg {
      name = "regex";
      version = "2.0";
      sha256 = "0qgqrrdr95yqggw8xyvb693nhizwqvf1fp9cjx9p3z80c4ih8j4j";
    };

    buildInputs = [
      
    ];
  };

  srfi-1 = eggDerivation {
    name = "srfi-1-0.5.1";

    src = fetchegg {
      name = "srfi-1";
      version = "0.5.1";
      sha256 = "15x0ajdkw5gb3vgs8flzh5g0pzl3wmcpf11iimlm67mw6fxc8p7j";
    };

    buildInputs = [
      
    ];
  };

  srfi-13 = eggDerivation {
    name = "srfi-13-0.3";

    src = fetchegg {
      name = "srfi-13";
      version = "0.3";
      sha256 = "0yaw9i6zhpxl1794pirh168clprjgmsb0xlr96drirjzsslgm3zp";
    };

    buildInputs = [
      srfi-14
    ];
  };

  srfi-14 = eggDerivation {
    name = "srfi-14-0.2.1";

    src = fetchegg {
      name = "srfi-14";
      version = "0.2.1";
      sha256 = "0gc33cx4xll9vsf7fm8jvn3gc0604kn3bbi6jfn6xscqp86kqb9p";
    };

    buildInputs = [
      
    ];
  };

  srfi-151 = eggDerivation {
    name = "srfi-151-1.0.2";

    src = fetchegg {
      name = "srfi-151";
      version = "1.0.2";
      sha256 = "0hhwcm38a2n65bi0xszscv9izkcwn7qcz2azppivdnch4z91xrg3";
    };

    buildInputs = [
      
    ];
  };

  srfi-63 = eggDerivation {
    name = "srfi-63-0.5";

    src = fetchegg {
      name = "srfi-63";
      version = "0.5";
      sha256 = "0db3vfac2p2zfmnw0m3gcixq4zyp60dil6gpwhh4nybkw1m14jv7";
    };

    buildInputs = [
      records
    ];
  };

  srfi-69 = eggDerivation {
    name = "srfi-69-0.4.1";

    src = fetchegg {
      name = "srfi-69";
      version = "0.4.1";
      sha256 = "1l102kppncz27acsr4jyi46q0r7g2lb6gdbkd6p3h1xmvwcnk2hl";
    };

    buildInputs = [
      
    ];
  };

  trie = eggDerivation {
    name = "trie-2";

    src = fetchegg {
      name = "trie";
      version = "2";
      sha256 = "14pbwxn42ahq5vsfw36fmkhxd4kf86p6vhkbzd7529bafv135nwi";
    };

    buildInputs = [
      srfi-1
    ];
  };

  utf8 = eggDerivation {
    name = "utf8-3.6.2";

    src = fetchegg {
      name = "utf8";
      version = "3.6.2";
      sha256 = "10wzp3qmwik4gx3hhaqm2n83wza0rllgy57363h5ccv8fga5nnm6";
    };

    buildInputs = [
      srfi-69
      iset
      regex
    ];
  };
}

